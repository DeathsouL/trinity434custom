DELETE FROM `spell_proc_event` WHERE `entry` IN (87099, 87100);
INSERT INTO `spell_proc_event` VALUES 
(87099, 0, 0, 0x00000000, 0x00000000, 0x00000440, 0x00010000, 0x00000002, 0, 0, 0), -- Sin and Punishment 
(87100, 0, 0, 0x00000000, 0x00000000, 0x00000440, 0x00010000, 0x00000002, 0, 0, 0);
DELETE FROM `spell_proc_event` WHERE `entry` IN (87099, 87100);
INSERT INTO `spell_proc_event` VALUES 
(87099, 0, 0, 0x00000000, 0x00000000, 0x00000440, 0x00040000, 0x00000002, 0, 0, 0), -- Sin and Punishment 
(87100, 0, 0, 0x00000000, 0x00000000, 0x00000440, 0x00040000, 0x00000002, 0, 0, 0);
DELETE FROM `spell_bonus_data` WHERE `entry` IN (15407, 589, 8092, 2944, 32379, 48045, 73510, 34914, 585);
INSERT INTO `spell_bonus_data`  VALUES 
(15407, 0, 0.288, 0, 0, 'priest- mind flay'),
(589, 0, 0.161, 0, 0, 'priest- shadow word pain'),
(8092, 1.104, 0, 0, 0, 'priest- mind blast'),
(2944, 0, 0.163, 0, 0, 'priest- devouring plague'),
(32379, 0.316, 0, 0, 0, 'priest- shadow word death'),
(48045, 0, 0.2622, 0, 0, 'priest- mind sear'),
(73510, 0.836, 0, 0, 0, 'priest- mind spike'),
(34914, 0, 0.352, 0, 0, 'priest- vampiric touch'),
(585, 0.856, 0, 0, 0, 'priest- smite');