-- http://es.wowhead.com/quest=13828
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0  WHERE `Id`= 13828;

-- http://es.wowhead.com/quest=13835/dominio-de-romper-escudo
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0  WHERE `Id`= 13835;

-- http://es.wowhead.com/quest=13837/dominio-de-cargar
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0  WHERE `Id`= 13837;

-- http://es.wowhead.com/quest=13666/una-espada-de-campeon�
UPDATE `quest_template` SET `RequiredItemCount1`= 0  WHERE `Id`= 13666;

-- http://es.wowhead.com/quest=13625/aprende-a-llevar-las-riendas
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0 WHERE `Id`= 13625;

-- http://es.wowhead.com/quest=13664/la-caida-del-caballero-negro
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
( NULL, 33785, 571, 1, 1, 0, 0, 8432.537109, 966.773560, 544.675171, 4.555926, 180, 0, 0, 50000, 7988, 0, 0, 0, 0 );
UPDATE `creature_template` SET `unit_flags`= 0 WHERE `entry`= 33785;

-- http://www.wowhead.com/quest=14016/la-maldicion-del-caballero-negro
UPDATE `quest_template` SET `Method`= 0  WHERE `Id`= 14016;

-- http://es.wowhead.com/quest=14101/drottinn-hrothgar
UPDATE `creature_template` SET `unit_flags`= 0 WHERE `entry`= 34980;

-- http://es.wowhead.com/quest=13679/el-desafio-del-aspirante
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 13679;

-- http://es.wowhead.com/quest=9962
UPDATE `quest_template` SET `SpecialFlags`=0, `RequiredNpcOrGo1`=18398, `RequiredNpcOrGoCount1`=1 WHERE `Id`=9962;

-- http://es.wowhead.com/quest=9967
UPDATE `quest_template` SET `SpecialFlags`=0, `RequiredNpcOrGo1`=18399, `RequiredNpcOrGoCount1`=1 WHERE `Id`=9967;

-- http://es.wowhead.com/quest=9970
UPDATE `quest_template` SET `SpecialFlags`=0, `RequiredNpcOrGo1`=18400, `RequiredNpcOrGoCount1`=1 WHERE `Id`=9970;

-- http://es.wowhead.com/quest=9972
UPDATE `quest_template` SET `SpecialFlags`=0, `RequiredNpcOrGo1`=18401, `RequiredNpcOrGoCount1`=1 WHERE `Id`=9972;

-- http://es.wowhead.com/quest=9973
UPDATE `quest_template` SET `SpecialFlags`=0, `RequiredNpcOrGo1`=18402, `RequiredNpcOrGoCount1`=1 WHERE `Id`=9973;

-- http://es.wowhead.com/quest=9977
UPDATE `quest_template` SET `SpecialFlags`=0, `RequiredNpcOrGo1`=18069, `RequiredNpcOrGoCount1`=1 WHERE `Id`=9977;

-- http://es.wowhead.com/quest=28862
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
(NULL, 48010, 0, 1, 1, 0, 0, -3184.653564, -50.637695, 120.520805, 3.557736, 300, 0, 0, 70946, 0, 0, 0, 0, 0 );

-- http://es.wowhead.com/quest=10477 siempre debe caer el item "25433" tengan o no tengan la mision.
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(17136, 25433, 100, 1, 0, 1, 1),
(17137, 25433, 100, 1, 0, 1, 1),
(18064, 25433, 100, 1, 0, 1, 1),
(17138, 25433, 100, 1, 0, 1, 1),
(17134, 25433, 100, 1, 0, 1, 1),
(17135, 25433, 100, 1, 0, 1, 1),
(18065, 25433, 100, 1, 0, 1, 1),
(18037, 25433, 100, 1, 0, 1, 1),
(18413, 25433, 100, 1, 0, 1, 1),
(18352, 25433, 100, 1, 0, 1, 1),
(18423, 25433, 100, 1, 0, 1, 1),
(18351, 25433, 100, 1, 0, 1, 1);


-- http://es.wowhead.com/quest=10019 siempre debe caer el item "25802" tengan o no tengan la mision.
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(18467, 25802, 100, 1, 0, 1, 1),
(18466, 25802, 100, 1, 0, 1, 1);

-- http://es.wowhead.com/quest=11085
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
( NULL, 23383, 530, 1, 1, 0, 0, -3388.710449, 3579.586670, 275.933258, 1.659371, 300, 0, 0, 6986, 0, 0, 0, 0, 0 );

-- http://es.wowhead.com/quest=11073
UPDATE `quest_template` SET `PrevQuestId`= 0 WHERE `Id`= 11073;
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
( NULL, 21838, 530, 1, 1, 0, 0, -3785.479980, 3507.024170, 287.007751, 2.964420, 300, 0, 0, 221400, 0, 0, 0, 0, 0 );

-- http://es.wowhead.com/quest=11096/amenaza-desde-el-cielo con esta quest se libera toda la cadena
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
( NULL, 23449, 530, 1, 1, 0, 0, -3401.090088, 3597.870117, 277.411987, 0.244346, 300, 0, 0, 6986, 0, 0, 0, 0, 0 );

-- http://es.wowhead.com/quest=11093
UPDATE `quest_template` SET `RequiredNpcOrGoCount2`= 0 WHERE `Id`= 11093;
-- http://es.wowhead.com/quest=11005
UPDATE `quest_template` SET `PrevQuestId`= 0 WHERE `Id`= 11005;

-- http://es.wowhead.com/quest=11021
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(23066, 32523, 100, 1, 0, 1, 1);

-- http://es.wowhead.com/quest=28824
UPDATE `quest_template` SET `PrevQuestId`= 27123 WHERE `Id`= 28824;

-- http://es.wowhead.com/quest=26709

UPDATE `quest_template` SET `PrevQuestId`= 27123 WHERE `Id`= 26709;

-- http://es.wowhead.com/quest=26326
UPDATE `quest_template` SET `PrevQuestId`= 27123 WHERE `Id`= 26326;

-- http://es.wowhead.com/npc=42731
UPDATE `quest_template` SET `PrevQuestId`= 27123 WHERE `Id`= 26326;

-- http://es.wowhead.com/quest=26312/defensas-desmoronadas
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0, `RequiredNpcOrGoCount3`= 0 WHERE `Id`= 26312;

-- http://es.wowhead.com/quest=26314/allanar-el-terreno
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 26314;

-- http://es.wowhead.com/quest=26315/enfrentamiento-imponente
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 26315;

-- http://es.wowhead.com/quest=26377/terreno-fragil
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 26377;

-- http://es.wowhead.com/quest=26426/vendaval-violento
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredNpcOrGoCount2`= 0  WHERE `Id`= 26426;

-- http://es.wowhead.com/quest=26426/vendaval-violento
INSERT INTO creature (guid, id, map, spawnmask, phaseMask, modelid, equipment_id, position_x, position_y, position_z, orientation, spawntimesecs, spawndist, currentwaypoint, curhealth, curmana, MovementType, npcflag, unit_flags, dynamicflags) VALUES
( NULL, 42467, 646, 1, 1, 0, 0, 1903.163940, 43.935188, -114.113892, 1.025367, 120, 0, 0, 2684050, 0, 0, 0, 0, 0 );

-- http://es.wowhead.com/quest=26869
INSERT INTO gameobject (guid, id, map, spawnMask, phaseMask, position_x, position_y, position_z, orientation, rotation0, rotation1, rotation2, rotation3, spawntimesecs, animprogress, state) VALUES
(NULL, 204959, 646, 1, 1, 2088.107910, -326.580536, -159.991379, 4.871529, 0, 0, 0, 0, 30, 100, 1 );

-- http://es.wowhead.com/quest=26437/limpieza-cristalina
INSERT INTO creature_loot_template (entry, item, ChanceOrQuestChance, lootmode, groupid, mincountOrRef, maxcount) VALUES
(42524, 58845, -100, 1, 0, 1, 1);

-- http://es.wowhead.com/quest=26439/reunir-todas-las-piezas
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0 WHERE `Id`= 26439;

-- http://es.wowhead.com/quest=10776/disension-en-las-filas
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredItemCount2`= 0 WHERE `Id`= 10776;

-- http://es.wowhead.com/quest=10769/disension-en-las-filas
UPDATE `quest_template` SET `RequiredNpcOrGoCount1`= 0, `RequiredItemCount2`= 0 WHERE `Id`= 10769;