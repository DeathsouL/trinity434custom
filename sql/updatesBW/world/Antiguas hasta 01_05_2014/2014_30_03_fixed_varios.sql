-- Glifo de disparo silenciador Hunter

DELETE FROM spell_script_names WHERE spell_id=34490;
INSERT INTO `spell_script_names` VALUES (34490, 'spell_hun_silencing_shot');

-- Especializacion en Escudos Warrior proteccion

DELETE FROM spell_proc_event WHERE `entry` IN (12298, 12724, 12725);
INSERT INTO `spell_proc_event` VALUES (12298, 0, 0, 0, 0, 0, 131752, 2112, 0, 100, 0);
INSERT INTO `spell_proc_event` VALUES (12724, 0, 0, 0, 0, 0, 131752, 2112, 0, 100, 0);
INSERT INTO `spell_proc_event` VALUES (12725, 0, 0, 0, 0, 0, 131752, 2112, 0, 100, 0);

-- Ascuas ardientes Brujo Destruccion

DELETE FROM spell_proc_event WHERE entry IN (12311,12958);
INSERT INTO spell_proc_event() VALUES
(12311,0,4,8,1,0,16,0,0,50,0),
(12958,0,4,8,1,0,16,0,0,100,0);

-- Proteccion Abisal Brujo

DELETE FROM spell_proc_event WHERE entry in(30299, 30301);
INSERT INTO `spell_proc_event` VALUES (30299, 0, 0, 0, 0, 0, 664096, 1024, 0, 100, 0);
INSERT INTO `spell_proc_event` VALUES (30301, 0, 0, 0, 0, 0, 664096, 1024, 0, 100, 0);

DELETE FROM spell_script_names WHERE ScriptName='spell_warl_nether_protection';
INSERT INTO `spell_script_names` VALUES (30299, 'spell_warl_nether_protection');
INSERT INTO `spell_script_names` VALUES (30301, 'spell_warl_nether_protection');

DELETE FROM spell_script_names WHERE ScriptName='spell_warl_shadow_ward';
DELETE FROM spell_script_names WHERE spell_id=7812;
INSERT INTO `spell_script_names` VALUES (7812, 'spell_warl_shadow_ward');
DELETE FROM spell_script_names WHERE spell_id=6229;
INSERT INTO `spell_script_names` VALUES (6229, 'spell_warl_shadow_ward');
DELETE FROM `spell_script_names` WHERE `spell_id`=91711;
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (91711, 'spell_warl_shadow_ward');
/*INSERT INTO `spell_script_names` VALUES (-7235, 'spell_warl_shadow_ward'); original */

/* -- improved lava lash

DELETE FROM spell_script_names WHERE spell_id in(77700, 77701);
INSERT INTO `spell_script_names` VALUES (77700, 'spell_sha_improved_lava_lash');
INSERT INTO `spell_script_names` VALUES (77701, 'spell_sha_improved_lava_lash');

DELETE FROM spell_proc_event WHERE entry in(77700, 77701);
INSERT INTO `spell_proc_event` VALUES (77700, 0, 11, 0, 0, 4, 16, 0, 0, 100, 0);
INSERT INTO `spell_proc_event` VALUES (77701, 0, 11, 0, 0, 4, 16, 0, 0, 100, 0);

DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=105792 AND `spell_effect`=8050;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (105792, 8050, 1, 'Improved Lava Lash applies Flame Shock');

*/