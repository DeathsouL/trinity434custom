-- Mana Shield
DELETE FROM `spell_script_names` WHERE `spell_id`=1463 OR `ScriptName`='spell_mage_mana_shield' OR `ScriptName`='spell_mage_incanters_absorbtion_manashield';
INSERT INTO `spell_script_names` VALUES
(1463,'spell_mage_mana_shield');

INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (66, 'spell_mage_invisibility_fading');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (32612, 'spell_mage_invisibility_invisible');