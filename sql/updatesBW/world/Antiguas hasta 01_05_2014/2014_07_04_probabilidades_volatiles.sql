/*
SQLyog Ultimate v11.4 (64 bit)
MySQL - 5.6.16 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

-- Con esto se mejora la subida de profesiones ya que aveces es muy dificil sacar los volatiles de verdad tienen drops muy bajos
-- para la poblacion que ya tenemos

-- Probabilidades fuego volatil
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52325 ;
UPDATE `item_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52325 ;
UPDATE `fishing_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52325 ;
UPDATE `gameobject_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52325 ;

-- Probabilidades aire volatil
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52328 ;
UPDATE `item_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52328 ;
UPDATE `fishing_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52328 ;
UPDATE `gameobject_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52328 ;

-- Probabilidades tierra volatil
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52327 ;
UPDATE `item_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52327 ;
UPDATE `fishing_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52327 ;
UPDATE `gameobject_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52327 ;

-- Probabilidades vida volatil
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52329 ;
UPDATE `item_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52329 ;
UPDATE `fishing_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52329 ;
UPDATE `gameobject_loot_template` SET `ChanceOrQuestChance`= 50 WHERE `item` = 52329 ;
