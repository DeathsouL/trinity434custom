-- FIX DE SPELL / TALENTOS CHAMAN 


-- FIX TOTEM ENLACE DE ESPIRITU

-- script name
DELETE FROM `spell_script_names` WHERE `spell_id`=98020;
DELETE FROM `spell_script_names` WHERE `ScriptName` = 'spell_sha_spirit_link';
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (98020, 'spell_sha_spirit_link');

-- addon template 
DELETE FROM `creature_template_addon` WHERE `entry`= 53006;
INSERT INTO `creature_template_addon` (`entry`, `auras`) VALUES (53006, '98017 98007');

-- FIX PODER CONGELADO

DELETE FROM `spell_script_names` WHERE `spell_id`=8056;

INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES
(8056,"spell_sha_frost_shock");

-- FIX SHAMAN FULMINACION

-- bonus data proc fulminacion

DELETE FROM `spell_bonus_data` WHERE (`entry`= 88767);
INSERT INTO `spell_bonus_data` (`entry`, `comments`) VALUES ('88767', 'shaman- fulmination proc');

-- scriptname Fulminacion
DELETE FROM `spell_script_names` WHERE `spell_id` IN (8042);
DELETE FROM `spell_script_names` WHERE ScriptName = 'spell_sha_fulmination';
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(8042, 'spell_sha_fulmination');

-- FIX LLAMAS ABRAZADORAS SHAMAN

-- llamas abrazadoras rank 1,2,3
DELETE FROM `spell_script_names` WHERE `spell_id`=3606;
DELETE FROM `spell_script_names` WHERE ScriptName = 'spell_gen_searing_bolt';
INSERT INTO `spell_script_names` VALUES
(3606,'spell_gen_searing_bolt');

-- daño llamas abrazadoras
DELETE FROM spell_bonus_data WHERE entry=77661;
INSERT INTO `spell_bonus_data` VALUES (77661, 0, 0.1667, 0, 0, 'Shaman - Searing Flames');

-- FIX TALENTO CHAMAN Trueno ensordecedor

-- script name
DELETE FROM `spell_script_names` WHERE `spell_id` IN (88756,88764);
DELETE FROM `spell_script_names` WHERE `ScriptName` = 'spell_sha_rolling_thunder';
INSERT INTO `spell_script_names`(`spell_id`, `ScriptName`) VALUES
(88756, 'spell_sha_rolling_thunder'),
(88764, 'spell_sha_rolling_thunder');

-- spell_proc_Event 
DELETE FROM `spell_proc_event` WHERE  `entry` IN (88756, 88764);
INSERT INTO `spell_proc_event` VALUES 
(88756, 0x0, 0x0, 0x00000003, 0x0, 0x0, 0x00010000, 0x0, 0, 0, 0),
(88764, 0x0, 0x0, 0x00000003, 0x0, 0x0, 0x00010000, 0x0, 0, 0, 0);