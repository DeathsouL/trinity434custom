ALTER TABLE `quest_template` ADD `RequiredSpellCast1` mediumint (8) AFTER RequiredSpell;
ALTER TABLE `quest_template` ADD `RequiredSpellCast2` mediumint (8) AFTER RequiredSpellCast1;
ALTER TABLE `quest_template` ADD `RequiredSpellCast3` mediumint (8) AFTER RequiredSpellCast2;
ALTER TABLE `quest_template` ADD `RequiredSpellCast4` mediumint (8) AFTER RequiredSpellCast3;