-- renato npc de ventormenta que daba wowerror al atacarlo

UPDATE creature_Template SET faction_A = 35 WHERE entry = 1432;
UPDATE creature_Template SET faction_H = 35 WHERE entry = 1432;

-- actualizar el tiempo de respawn de alma de dragon

UPDATE creature SET spawntimesecs = 172800 WHERE map = 967;

-- congelacion profunda proc de inmunidades
DELETE FROM `spell_proc_event` WHERE `entry`=71761;
INSERT INTO `spell_proc_event` VALUES
(71761,0,3,0,1048576,0,65536,0,0,0,0);


--- tauren shaman fix de raciales
 UPDATE `playercreateinfo_spell` SET `classmask`=1143 WHERE (`racemask`=32) AND (`Spell`=20552);
 UPDATE `playercreateinfo_spell` SET `classmask`=1143 WHERE (`racemask`=32) AND (`Spell`=20550);
 UPDATE `playercreateinfo_spell` SET `classmask`=1143 WHERE (`racemask`=32) AND (`Spell`=79746);
 UPDATE `playercreateinfo_spell` SET `classmask`=1143 WHERE (`racemask`=32) AND (`Spell`=20551);
 UPDATE `playercreateinfo_spell` SET `classmask`=1143 WHERE (`racemask`=32) AND (`Spell`=20549);

-- Bendicion de poderio y bendicion de reyes buff fix pala
 DELETE FROM `spell_linked_spell` WHERE `spell_trigger` IN(79063,79102);
 INSERT INTO `spell_linked_spell` (`spell_trigger`,`spell_effect`,`type`,`req_aura`,`comment`) VALUES
 (79063,-79102,1,0,"Blessing of Kings"),
 (79102,-79063,1,0,"Blessing of Might");

-- item solo se puede ocupar en zuldrak (rasganorte)

INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `params_0`, `params_1`, `comment`) VALUES
('0','50894','17','0,1,13,25,30,33,34,35,36,37,42,43,44,47,48,70,90,109,129,169,189,209,229,230,249,269,289,309,329,349,369,389,409,429,449,450,451,469,489,509,529,530,531,532,533,534,540,542,543,544,545,546,547,548,550,552,553,554,555,556,557,558,559,560,562,564,565,566,568,572,573,574,575,576,578,580,582,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,600,601,602,603,604,605,606,607,608,609,610,612,613,614,615,616,617,618,619,620,621,622,623,624,627,628,631,632,637,638,641,642,643,644,645,646,647,648,649,650,651,654,655,656,657,658,659,660,661,662,668,669,670,671,672,673,674,712,713,718,719,720,721,723,724,725,726,727,728,730,731,732,734,736,738,739,740,741,742,743,746,747,748,749,750,751,752,753,754,755,757,759,760,761,762,763,764,765,766,767,859,861,930,938,939,940,951,967,968,974,977,980','0','Spell activada por item solo en map 571 Rasganorte');


-- fixes ulduar
-- http://es.wowhead.com/object=194569 Teleport de Ulduar.
UPDATE `gameobject_template` SET `flags`=32 WHERE `entry`=194569;
-- http://es.wowhead.com/npc=33113 Leviat�n de llamas
UPDATE `creature_template` SET `difficulty_entry_1`=34003, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=33113;
-- http://es.wowhead.com/npc=33118 Ignis el Maestro de la Caldera
UPDATE `creature_template` SET `difficulty_entry_1`=33190, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=33118;
-- http://es.wowhead.com/npc=33186 Tajoescama
UPDATE `creature_template` SET `difficulty_entry_1`=33724, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=33186;
-- http://es.wowhead.com/npc=33293  Desarmador XA-002
UPDATE `creature_template` SET `difficulty_entry_1`=33885, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=33293;
-- http://es.wowhead.com/npc=32867 Rompeacero
UPDATE `creature_template` SET `difficulty_entry_1`=33693, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=33867;
-- http://es.wowhead.com/npc=32927 Maestro de runas Molgeim
UPDATE `creature_template` SET `difficulty_entry_1`=33692, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=32927;
-- http://es.wowhead.com/npc=32857  Clamatormentas Brundir
UPDATE `creature_template` SET `difficulty_entry_1`=33694, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=32857;
-- http://es.wowhead.com/npc=32930 Kologarn
UPDATE `creature_template` SET `difficulty_entry_1`=33909, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=32930;
-- http://es.wowhead.com/npc=32934 Brazo derecho
UPDATE `creature_template` SET `difficulty_entry_1`=33911, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=32934;
-- http://es.wowhead.com/npc=32933  Brazo izquierdo
UPDATE `creature_template` SET `difficulty_entry_1`=33910, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=32933;
-- http://es.wowhead.com/npc=32871 Algalon el Observador
UPDATE `creature_template` SET `difficulty_entry_1`=33070, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=32871;
-- http://es.wowhead.com/npc=32845 Hodir
UPDATE `creature_template` SET `difficulty_entry_1`=32846, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=32845;
-- http://es.wowhead.com/npc=32913 Ancestro Hierrorrama
UPDATE `creature_template` SET `difficulty_entry_1`=33392, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=32913;
-- http://es.wowhead.com/npc=32914 Ancestro Cortezapiedra
UPDATE `creature_template` SET `difficulty_entry_1`=33393, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=32914;
-- http://es.wowhead.com/npc=33432 Mk II de leviat�n
UPDATE `creature_template` SET `difficulty_entry_1`=34106, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=33432;
-- http://es.wowhead.com/npc=33651  VX-001 <Ca��n de asalto antipersona>
UPDATE `creature_template` SET `difficulty_entry_1`=34108, `minlevel`=83, `maxlevel`=83, `exp`=0 WHERE `entry`=33651;
-- http://es.wowhead.com/npc=33670 Unidad de mando a�rea
UPDATE `creature_template` SET `difficulty_entry_1`=34109, `minlevel`=83, `maxlevel`=83, `exp`=0 WHERE `entry`=33670;
-- http://es.wowhead.com/npc=33350 Mimiron
UPDATE `creature_template` SET  `minlevel`=83, `maxlevel`=83, `exp`=0 WHERE `entry`=33350;
-- http://es.wowhead.com/npc=33271 General Vezax
UPDATE `creature_template` SET `difficulty_entry_1`=33449, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=33271;
-- http://es.wowhead.com/npc=33288  Yogg-Saron
UPDATE `creature_template` SET `difficulty_entry_1`=33955, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=33288;
-- http://es.wowhead.com/npc=33329/corazon-del-desarmador Coraz�n del Desarmador
UPDATE `creature_template` SET `difficulty_entry_1`=33955, `minlevel`=83, `maxlevel`=83, `exp`=2 WHERE `entry`=33329;


 --recetas de ingenieria descubrimiento 
 DELETE FROM skill_discovery_template WHERE spellId IN (84425, 84427, 84424, 82177, 82200, 82175, 82180, 82201);
 INSERT INTO skill_discovery_template (spellId, reqSpell, reqSkillValue, chance) VALUES
  --obsidium bolts 84403 
 (84425, 84403, 450, 5), 
 (84427, 84403, 450, 5), 
 (84424, 84403, 450, 5), 
 (82177, 84403, 450, 5), 
 (82200, 84403, 450, 5), 
 (82175, 84403, 450, 5), 
 (82180, 84403, 450, 5), 
 (82201, 84403, 450, 5),
 -- electrostatic sondenser 95703 
 (84425, 95703, 450, 5), 
 (84427, 95703, 450, 5), 
 (84424, 95703, 450, 5), 
 (82177, 95703, 450, 5), 
 (82200, 95703, 450, 5), 
 (82175, 95703, 450, 5), 
 (82180, 95703, 450, 5), 
 (82201, 95703, 450, 5),
--  authentic goggles 84406 
 (84425, 84406, 450, 5), 
 (84427, 84406, 450, 5), 
 (84424, 84406, 450, 5), 
 (82177, 84406, 450, 5), 
 (82200, 84406, 450, 5), 
 (82175, 84406, 450, 5), 
 (82180, 84406, 450, 5), 
 (82201, 84406, 450, 5),
--  electrified ether 94748 
 (84425, 94748, 450, 5), 
 (84427, 94748, 450, 5), 
 (84424, 94748, 450, 5), 
 (82177, 94748, 450, 5), 
 (82200, 94748, 450, 5), 
 (82175, 94748, 450, 5), 
 (82180, 94748, 450, 5), 
 (82201, 94748, 450, 5),
--  r19 theat 84408 
 (84425, 84408, 450, 5), 
 (84427, 84408, 450, 5), 
 (84424, 84408, 450, 5), 
 (82177, 84408, 450, 5), 
 (82200, 84408, 450, 5), 
 (82175, 84408, 450, 5), 
 (82180, 84408, 450, 5), 
 (82201, 84408, 450, 5),
 -- safe cacth removal 84410 
 (84425, 84410, 450, 5), 
 (84427, 84410, 450, 5), 
 (84424, 84410, 450, 5), 
 (82177, 84410, 450, 5), 
 (82200, 84410, 450, 5), 
 (82175, 84410, 450, 5), 
 (82180, 84410, 450, 5), 
 (82201, 84410, 450, 5),
 -- volatile c4 blastpack 84409 
 (84425, 84409, 450, 5), 
 (84427, 84409, 450, 5), 
 (84424, 84409, 450, 5), 
 (82177, 84409, 450, 5), 
 (82200, 84409, 450, 5), 
 (82175, 84409, 450, 5), 
 (82180, 84409, 450, 5), 
 (82201, 84409, 450, 5),
--  highpower gun 84411 
 (84425, 84411, 450, 5), 
 (84427, 84411, 450, 5), 
 (84424, 84411, 450, 5), 
 (82177, 84411, 450, 5), 
 (82200, 84411, 450, 5), 
 (82175, 84411, 450, 5), 
 (82180, 84411, 450, 5), 
 (82201, 84411, 450, 5),
 -- lure master box 84415 
 (84425, 84415, 450, 5), 
 (84427, 84415, 450, 5), 
 (84424, 84415, 450, 5), 
 (82177, 84415, 450, 5), 
 (82200, 84415, 450, 5), 
 (82175, 84415, 450, 5), 
 (82180, 84415, 450, 5), 
 (82201, 84415, 450, 5),
 -- deweaponized mechanical companion 84413 
 (84425, 84413, 450, 5), 
 (84427, 84413, 450, 5), 
 (84424, 84413, 450, 5), 
 (82177, 84413, 450, 5), 
 (82200, 84413, 450, 5), 
 (82175, 84413, 450, 5), 
 (82180, 84413, 450, 5), 
 (82201, 84413, 450, 5),
--  elementium toolbox 84416 
 (84425, 84416, 450, 5), 
 (84427, 84416, 450, 5), 
 (84424, 84416, 450, 5), 
 (82177, 84416, 450, 5), 
 (82200, 84416, 450, 5), 
 (82175, 84416, 450, 5), 
 (82180, 84416, 450, 5), 
 (82201, 84416, 450, 5),
--  personal world destroyer 84412 
 (84425, 84412, 450, 5), 
 (84427, 84412, 450, 5), 
 (84424, 84412, 450, 5), 
 (82177, 84412, 450, 5), 
 (82200, 84412, 450, 5), 
 (82175, 84412, 450, 5), 
 (82180, 84412, 450, 5), 
 (82201, 84412, 450, 5),
 -- elementium dragon 84418 
 (84425, 84418, 450, 5), 
 (84427, 84418, 450, 5), 
 (84424, 84418, 450, 5), 
 (82177, 84418, 450, 5), 
 (82200, 84418, 450, 5), 
 (82175, 84418, 450, 5), 
 (82180, 84418, 450, 5), 
 (82201, 84418, 450, 5),
 -- lootarang 84421 
 (84425, 84421, 450, 5), 
 (84427, 84421, 450, 5), 
 (84424, 84421, 450, 5), 
 (82177, 84421, 450, 5), 
 (82200, 84421, 450, 5), 
 (82175, 84421, 450, 5), 
 (82180, 84421, 450, 5), 
 (82201, 84421, 450, 5),
--  finelytuned throat needler 84420 
 (84425, 84420, 450, 5), 
 (84427, 84420, 450, 5), 
 (84424, 84420, 450, 5), 
 (82177, 84420, 450, 5), 
 (82200, 84420, 450, 5), 
 (82175, 84420, 450, 5), 
 (82180, 84420, 450, 5), 
 (82201, 84420, 450, 5),
--  volatile thunderstick 84417 
 (84425, 84417, 450, 5), 
 (84427, 84417, 450, 5), 
 (84424, 84417, 450, 5), 
 (82177, 84417, 450, 5), 
 (82200, 84417, 450, 5), 
 (82175, 84417, 450, 5), 
 (82180, 84417, 450, 5), 
 (82201, 84417, 450, 5),
--  gnomish gravity well 95705 
 (84425, 95705, 450, 5), 
 (84427, 95705, 450, 5), 
 (84424, 95705, 450, 5), 
 (82177, 95705, 450, 5), 
 (82200, 95705, 450, 5), 
 (82175, 95705, 450, 5), 
 (82180, 95705, 450, 5), 
 (82201, 95705, 450, 5),
--  big daddy 95707 
 (84425, 95707, 450, 5), 
 (84427, 95707, 450, 5), 
 (84424, 95707, 450, 5), 
 (82177, 95707, 450, 5), 
 (82200, 95707, 450, 5), 
 (82175, 95707, 450, 5), 
 (82180, 95707, 450, 5), 
 (82201, 95707, 450, 5),
--  goblin bbq 84429 
 (84425, 84429, 450, 5), 
 (84427, 84429, 450, 5), 
 (84424, 84429, 450, 5), 
 (82177, 84429, 450, 5), 
 (82200, 84429, 450, 5), 
 (82175, 84429, 450, 5), 
 (82180, 84429, 450, 5), 
 (82201, 84429, 450, 5),
--  heattreated lure 84430 
 (84425, 84430, 450, 5), 
 (84427, 84430, 450, 5), 
 (84424, 84430, 450, 5), 
 (82177, 84430, 450, 5), 
 (82200, 84430, 450, 5), 
 (82175, 84430, 450, 5), 
 (82180, 84430, 450, 5), 
 (82201, 84430, 450, 5),
--  gnomish xray scope 84428 
 (84425, 84428, 450, 5), 
 (84427, 84428, 450, 5), 
 (84424, 84428, 450, 5), 
 (82177, 84428, 450, 5), 
 (82200, 84428, 450, 5), 
 (82175, 84428, 450, 5), 
 (82180, 84428, 450, 5), 
 (82201, 84428, 450, 5),
--  kickback 5000 84432 
 (84425, 84432, 450, 5), 
 (84427, 84432, 450, 5), 
 (84424, 84432, 450, 5), 
 (82177, 84432, 450, 5), 
 (82200, 84432, 450, 5), 
 (82175, 84432, 450, 5), 
 (82180, 84432, 450, 5), 
 (82201, 84432, 450, 5),
--  agile killshades 81722 
 (84425, 81722, 450, 5), 
 (84427, 81722, 450, 5), 
 (84424, 81722, 450, 5), 
 (82177, 81722, 450, 5), 
 (82200, 81722, 450, 5), 
 (82175, 81722, 450, 5), 
 (82180, 81722, 450, 5), 
 (82201, 81722, 450, 5),
--  camoulflage killshades 81724 
 (84425, 81724, 450, 5), 
 (84427, 81724, 450, 5), 
 (84424, 81724, 450, 5), 
 (82177, 81724, 450, 5), 
 (82200, 81724, 450, 5), 
 (82175, 81724, 450, 5), 
 (82180, 81724, 450, 5), 
 (82201, 81724, 450, 5),
--  deadly killshades 81716 
 (84425, 81716, 450, 5), 
 (84427, 81716, 450, 5), 
 (84424, 81716, 450, 5), 
 (82177, 81716, 450, 5), 
 (82200, 81716, 450, 5), 
 (82175, 81716, 450, 5), 
 (82180, 81716, 450, 5), 
 (82201, 81716, 450, 5),
--  lightweight killshades 81725 
 (84425, 81725, 450, 5), 
 (84427, 81725, 450, 5), 
 (84424, 81725, 450, 5), 
 (82177, 81725, 450, 5), 
 (82200, 81725, 450, 5), 
 (82175, 81725, 450, 5), 
 (82180, 81725, 450, 5), 
 (82201, 81725, 450, 5),
--  reinforced killshades 81714 
 (84425, 81714, 450, 5), 
 (84427, 81714, 450, 5), 
 (84424, 81714, 450, 5), 
 (82177, 81714, 450, 5), 
 (82200, 81714, 450, 5), 
 (82175, 81714, 450, 5), 
 (82180, 81714, 450, 5), 
 (82201, 81714, 450, 5),
 -- specialized killshades 81715 
 (84425, 81715, 450, 5), 
 (84427, 81715, 450, 5), 
 (84424, 81715, 450, 5), 
 (82177, 81715, 450, 5), 
 (82200, 81715, 450, 5), 
 (82175, 81715, 450, 5), 
 (82180, 81715, 450, 5), 
 (82201, 81715, 450, 5),
--  energized killshades 81720 
 (84425, 81720, 450, 5), 
 (84427, 81720, 450, 5), 
 (84424, 81720, 450, 5), 
 (82177, 81720, 450, 5), 
 (82200, 81720, 450, 5), 
 (82175, 81720, 450, 5), 
 (82180, 81720, 450, 5), 
 (82201, 81720, 450, 5),
 -- chicken splitter 100% szansy 84431 
 (84425, 84431, 450, 100), 
 (84427, 84431, 450, 100), 
 (84424, 84431, 450, 100), 
 (82177, 84431, 450, 100), 
 (82200, 84431, 450, 100), 
 (82175, 84431, 450, 100), 
 (82180, 84431, 450, 100), 
 (82201, 84431, 450, 100);