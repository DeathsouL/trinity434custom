/*
* Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* Script SQLs

-- Intro Areatrigger
DELETE FROM `areatrigger_scripts` WHERE `entry`=7246;
INSERT INTO `areatrigger_scripts` (`entry`, `ScriptName`) VALUES
(7246, 'at_alizabal_intro');

UPDATE `creature_template` SET `scriptname` = 'boss_alizabal' WHERE `entry`=55869;

-- Todo: Find the Rest of the Texts and their meanings
DELETE FROM `creature_text` WHERE `entry`=55869;
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(55869, 1, 0, 'How I HATE this place. My captors may be long-dead, but don\'t think I won\'t take it all out on you miserable treasure-hunters.', 14, 0, 100, 0, 0, 25779, 'Alizabal - Intro'),
(55869, 2, 0, 'I hate adventurers.', 14, 0, 100, 0, 0, 25777, 'Alizabal - Aggro'),
(55869, 3, 0, 'I hate martyrs.', 14, 0, 100, 0, 0, 25786, 'Alizabal - Seething Hate'),
(55869, 4, 0, 'I hate armor.', 14, 0, 100, 0, 0, 25785, 'Alizabal - Skewer'),
(55869, 5, 0, 'Alizabal nailed $N firmly on the ground.', 41, 0, 100, 0, 0, 0, 'Alizabal - Skewer Announce'),
(55869, 6, 0, 'I hate standing still.', 14, 0, 100, 0, 0, 25791, 'Alizabal - Blade Dance'),
(55869, 7, 0, 'Feel my hatred!', 14, 0, 100, 0, 0, 25787, 'Alizabal - ???'),
(55869, 8, 0, 'My hate will consume you!', 14, 0, 100, 0, 0, 25789, 'Alizabal - ???'),
(55869, 9, 0, 'I hate you all.', 14, 0, 100, 0, 0, 25790, 'Alizabal - ???'),
(55869, 10, 0, 'I hate mercy.', 14, 0, 100, 0, 0, 25783, 'Alizabal - Slay 1'),
(55869, 11, 0, 'My hatred burns!', 14, 0, 100, 0, 0, 25788, 'Alizabal - ???'),
(55869, 12, 0, 'I hate... every one of you...', 14, 0, 100, 0, 0, 25778, 'Alizabal - Death');
*/

#include "baradin_hold.h"
#include "InstanceScript.h"
#include "ScriptMgr.h"
#include "Player.h"
#include "ObjectAccessor.h"
#include "ScriptedCreature.h"

enum Texts
{
    SAY_INTRO           = 1,
    SAY_AGGRO           = 2,
    SAY_HATE            = 3,
    SAY_SKEWER          = 4,
    SAY_SKEWER_ANNOUNCE = 5,
    SAY_BLADE_STORM     = 6,
    SAY_SLAY            = 10,
    SAY_DEATH           = 12,
};

enum Spells
{
    SPELL_BLADE_DANCE       = 105784,
    SPELL_BLADE_DANCE_DUMMY = 105828,
    SPELL_SEETHING_HATE     = 105067,
    SPELL_SKEWER            = 104936,
    SPELL_BERSERK           = 47008,
};

enum Actions
{
    ACTION_INTRO    = 1,
};

enum Points
{
    POINT_STORM     = 1,
};

enum Events
{
    EVENT_RANDOM_CAST   = 1,
    EVENT_STOP_STORM    = 2,
    EVENT_MOVE_STORM    = 3,
    EVENT_CAST_STORM    = 4,
};

class at_alizabal_intro : public AreaTriggerScript
{
    public:
        at_alizabal_intro() : AreaTriggerScript("at_alizabal_intro") { }

        bool OnTrigger(Player* player, AreaTriggerEntry const* /*areaTrigger*/)
        {
            if (InstanceScript* instance = player->GetInstanceScript())
                if (Creature* alizabal = ObjectAccessor::GetCreature(*player, instance->GetData64(DATA_ALIZABAL)))
                    alizabal->AI()->DoAction(ACTION_INTRO);
            return true;
        }
};

class boss_alizabal : public CreatureScript
{
    public:
        boss_alizabal() : CreatureScript("boss_alizabal") { }

        struct boss_alizabalAI : public BossAI
        {
            boss_alizabalAI(Creature* creature) : BossAI(creature, DATA_ALIZABAL)
            {
                Intro = false;
                Hate = false;
                Skewer = false;
            }

            bool Intro;
            bool Hate;
            bool Skewer;

            void Reset()
            {
                _Reset();
                Hate = false;
                Skewer = false;
            }

            void EnterCombat(Unit* /*who*/)
            {
                _EnterCombat();
                Talk(SAY_AGGRO);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, me);
                events.ScheduleEvent(EVENT_RANDOM_CAST, 10000);
            }

            void JustDied(Unit* /*killer*/)
            {
                _JustDied();
                Talk(SAY_DEATH);
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me);
            }

            void KilledUnit(Unit* who)
            {
                if (who->GetTypeId() == TYPEID_PLAYER)
                    Talk(SAY_SLAY);
            }

            void EnterEvadeMode()
            {
                instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me);
                me->GetMotionMaster()->MoveTargetedHome();
                _DespawnAtEvade();
            }

            void DoAction(int32 action)
            {
                switch (action)
                {
                    case ACTION_INTRO:
                        if (!Intro)
                        {
                            Talk(SAY_INTRO);
                            Intro = true;
                        }
                        break;
                }
            }

            void MovementInform(uint32 /*type*/, uint32 pointId)
            {
                switch (pointId)
                {
                    case POINT_STORM:
                        events.ScheduleEvent(EVENT_CAST_STORM, 1);
                        break;
                }
            }

            void UpdateAI(uint32 diff)
            {
                if (!UpdateVictim())
                    return;

                events.Update(diff);

                while (uint32 eventId = events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_RANDOM_CAST:
                            switch (urand(0, 1))
                            {
                                case 0:
                                    if (!Skewer)
                                    {
                                        if (Unit* target = SelectTarget(SELECT_TARGET_TOPAGGRO, 0))
                                        {
                                            DoCast(target, SPELL_SKEWER, true);
                                            Talk(SAY_SKEWER);
                                            Talk(SAY_SKEWER_ANNOUNCE, target->GetGUID());
                                        }
                                        Skewer = true;
                                        events.ScheduleEvent(EVENT_RANDOM_CAST, urand(7000, 10000));
                                    }
                                    else if (!Hate)
                                    {
                                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, NonTankTargetSelector(me)))
                                        {
                                            DoCast(target, SPELL_SEETHING_HATE, true);
                                            Talk(SAY_HATE);
                                        }
                                        Hate = true;
                                        events.ScheduleEvent(EVENT_RANDOM_CAST, urand(7000, 10000));
                                    }
                                    else if (Hate && Skewer)
                                    {
                                        Talk(SAY_BLADE_STORM);
                                        DoCastAOE(SPELL_BLADE_DANCE_DUMMY);
                                        DoCastAOE(SPELL_BLADE_DANCE);
                                        events.ScheduleEvent(EVENT_RANDOM_CAST, 21000);
                                        events.ScheduleEvent(EVENT_MOVE_STORM, 4050);
                                        events.ScheduleEvent(EVENT_STOP_STORM, 13000);
                                    }
                                    break;
                                case 1:
                                    if (!Hate)
                                    {
                                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, NonTankTargetSelector(me)))
                                        {
                                            DoCast(target, SPELL_SEETHING_HATE, true);
                                            Talk(SAY_HATE);
                                        }
                                        Hate = true;
                                        events.ScheduleEvent(EVENT_RANDOM_CAST, urand(7000, 10000));
                                    }
                                    else if (!Skewer)
                                    {
                                        if (Unit* target = SelectTarget(SELECT_TARGET_TOPAGGRO, 0))
                                        {
                                            DoCast(target, SPELL_SKEWER, true);
                                            Talk(SAY_SKEWER);
                                            Talk(SAY_SKEWER_ANNOUNCE, target->GetGUID());
                                        }
                                        Skewer = true;
                                        events.ScheduleEvent(EVENT_RANDOM_CAST, urand(7000, 10000));
                                    }
                                    else if (Hate && Skewer)
                                    {
                                        Talk(SAY_BLADE_STORM);
                                        DoCastAOE(SPELL_BLADE_DANCE_DUMMY);
                                        DoCastAOE(SPELL_BLADE_DANCE);
                                        events.ScheduleEvent(EVENT_RANDOM_CAST, 21000);
                                        events.ScheduleEvent(EVENT_MOVE_STORM, 4050);
                                        events.ScheduleEvent(EVENT_STOP_STORM, 13000);
                                    }
                                    break;
                            }
                            break;
                        case EVENT_MOVE_STORM:
                            me->SetSpeed(MOVE_RUN, 4.0f);
                            me->SetSpeed(MOVE_WALK, 4.0f);
                            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, NonTankTargetSelector(me)))
                                me->GetMotionMaster()->MovePoint(POINT_STORM, target->GetPositionX(), target->GetPositionY(), target->GetPositionZ());
                            events.ScheduleEvent(EVENT_MOVE_STORM, 4050);
                            break;
                        case EVENT_STOP_STORM:
                            me->RemoveAura(SPELL_BLADE_DANCE);
                            me->RemoveAura(SPELL_BLADE_DANCE_DUMMY);
                            me->SetSpeed(MOVE_WALK, 1.0f);
                            me->SetSpeed(MOVE_RUN, 1.14f);
                            me->GetMotionMaster()->MoveChase(me->GetVictim());
                            Hate = false;
                            Skewer = false;
                            break;
                        case EVENT_CAST_STORM:
                            DoCastAOE(SPELL_BLADE_DANCE);
                            break;
                    }
                }

                DoMeleeAttackIfReady();
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss_alizabalAI(creature);
        }
};

void AddSC_boss_alizabal()
{
    new boss_alizabal();
    new at_alizabal_intro();
}
