/*
 * Copyright (C) 2013-2014 Sovak <sovak007@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "end_time.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"

enum Spells
{
    SPELL_PULVERIZE_VIS     = 101625,
    SPELL_PULVERIZE         = 101626,
    SPELL_PULVERIZE_DAMAGE  = 101627,
    SPELL_THROW_TOTEM       = 101614,
    SPELL_BAINE_TOTEM       = 101594,
    SPELL_BAINE_VISUAL      = 101624,
    SPELL_MOLTEN_BLAST      = 101840
};

enum Events
{
    EVENT_PULVERIZE         = 1,
    EVENT_THROW_TOTEM       = 2
};

enum GameObjects
{
    GAMEOBJECT_PLATFORM     = 209255
};

const Position platforms[4] =
{
    {4405.74, 1445.94, 129.932, 3.6120 },
    {4374.46, 1485.07, 129.932, 4.61813 },
    {4352.58, 1452.13, 129.933, 3.69529 },
    {4377.19, 1410.14, 129.932, 0.356563 }
};

const float pos3[4] = {0.972463, 0.739635, 0.961921, 0.177339 };
const float pos4[4] = {-0.233059, -0.673009, -0.273326, 0.98415 };

enum CreatureText
{
    SAY_INTRO   = 0,
    SAY_AGGRO   = 1,
    SAY_SPELL1  = 2,
    SAY_SPELL2  = 3,
    SAY_SLAY1   = 4,
    SAY_SLAY2   = 5,
    SAY_SLAY3   = 6,
    SAY_DEATH   = 8
};

class boss_echo_of_baine : public CreatureScript
{
public:
    boss_echo_of_baine() : CreatureScript("boss_echo_of_baine") 
    {
    }

    BossAI* GetAI(Creature* creature) const
    {
        return new boss_echo_of_baineAI(creature);
    }

    struct boss_echo_of_baineAI : public BossAI
    {
        boss_echo_of_baineAI(Creature* creature) : BossAI(creature, DATA_ECHO_OF_BAINE)
        {
            instance = me->GetInstanceScript();
        }

        InstanceScript* instance;
        uint32 timer;
        bool saidIntro;

        void Reset()
        {
            saidIntro = false;
            timer = 2000;
            me->AddAura(SPELL_BAINE_VISUAL, me);

            std::list<GameObject*> gobList;
            gobList = GetAvailablePlatforms();

            if (gobList.size() != 4)
            {
                for (std::list<GameObject*>::const_iterator iter = gobList.begin(); iter != gobList.end(); iter++)
                    if (GameObject* gobject = *iter)
                        gobject->Delete();

                for (int i = 0; i < 4; i++)
                    me->SummonGameObject(GAMEOBJECT_PLATFORM, platforms[i].GetPositionX(), platforms[i].GetPositionY(), platforms[i].GetPositionZ(), platforms[i].GetOrientation(), 1.93379E-43, 0, pos3[i], pos4[i], 600);
            }
        }

        void MoveInLineOfSight(Unit* who)
        {
            if (who->ToPlayer())
            {
                if (me->IsWithinDist(who, 50.f, true) && !saidIntro)
                {
                    saidIntro = true;
                    Talk(SAY_INTRO);
                }
            }
        }

        void EnterEvadeMode()
        {
            if (instance)
                instance->SetBossState(DATA_ECHO_OF_BAINE, FAIL);

            me->GetMotionMaster()->MoveTargetedHome();
            Reset();
        }

        void EnterCombat(Unit* who) 
        {
            if (instance)
                instance->SetBossState(DATA_ECHO_OF_BAINE, IN_PROGRESS);

            events.Reset();
            events.ScheduleEvent(EVENT_PULVERIZE, 40000);
            events.ScheduleEvent(EVENT_THROW_TOTEM, 25000);
        }

        void KilledUnit(Unit* who)
        {
            Talk(urand(SAY_SLAY1, SAY_SLAY3));
        }

        void JustDied(Unit* killer)
        {
            if (instance)
                instance->SetBossState(DATA_ECHO_OF_BAINE, DONE);

            Talk(SAY_DEATH);
        }

        std::list<GameObject*> GetAvailablePlatforms()
        {
            CellCoord p(Trinity::ComputeCellCoord(me->GetPositionX(), me->GetPositionY()));
            Cell cell(p);
            cell.SetNoCreate();

            std::list<GameObject*> gobList;

            Trinity::AllGameObjectsWithEntryInRange check(me, GAMEOBJECT_PLATFORM, 300.f);
            Trinity::GameObjectListSearcher<Trinity::AllGameObjectsWithEntryInRange> searcher(me, gobList, check);

            TypeContainerVisitor<Trinity::GameObjectListSearcher<Trinity::AllGameObjectsWithEntryInRange>, GridTypeMapContainer >  gridGobjectSearcher(searcher);

            cell.Visit(p, gridGobjectSearcher, *me->GetMap(), *me, 300.f);
            return gobList;
        }

        GameObject* CalculatePlatform()
        {
            std::list<GameObject*> platforms = GetAvailablePlatforms();
            GameObject* gobj = NULL;
            float dist = 500.f;

            if (platforms.size() == 1)
                return platforms.front();

            if (platforms.size())
            {
                for (std::list<GameObject*>::const_iterator iter = platforms.begin(); iter != platforms.end(); iter++)
                {
                    float currentDist = me->GetDistance2d(*iter);
                    if (dist > currentDist)
                    {
                        dist = currentDist;
                        gobj = *iter;
                    }
                }
            }
            return gobj;
        }

        void UpdateAI(uint32 diff)
        {
            if (!UpdateVictim())
                return;

            if (timer <= diff)
            {
                if (me->GetMap()->IsInWater(me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), 0))
                    me->AddAura(SPELL_MOLTEN_BLAST, me);
                timer = 2000;
            }
            else
                timer -= diff;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_PULVERIZE:
                    {
                        Talk(SAY_SPELL1);
                        GameObject* platform = CalculatePlatform();
                        if (platform)
                        {
                            me->CastStop();
                            me->CastSpell(platform->GetPositionX(), platform->GetPositionY(), platform->GetPositionZ(), SPELL_PULVERIZE, false);
                            me->CastSpell(platform->GetPositionX(), platform->GetPositionY(), platform->GetPositionZ(), SPELL_PULVERIZE_VIS, false);
                        }
                        events.ScheduleEvent(EVENT_PULVERIZE, 40000);
                        break;
                    }
                    case EVENT_THROW_TOTEM:
                    {
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 200, true))
                            me->CastSpell(target, SPELL_THROW_TOTEM, true);
                        events.ScheduleEvent(EVENT_THROW_TOTEM, 25000);
                        break;
                    }
                }
            }
            DoMeleeAttackIfReady();
        }
    };
};

class spell_echo_of_baine_pulverize : public SpellScriptLoader
{
    public:
        spell_echo_of_baine_pulverize() : SpellScriptLoader("spell_echo_of_baine_pulverize") { }

        class spell_echo_of_baine_pulverize_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_echo_of_baine_pulverize_SpellScript);

            void HandleAfterJump(SpellEffIndex effIndex)
            {
                Unit* caster = GetCaster();

                if (!caster->ToCreature())
                    return;

                GameObject* gobj = NULL;
                std::list<GameObject*> gobList;
                float dist = 300.f;

                CellCoord p(Trinity::ComputeCellCoord(caster->GetPositionX(), caster->GetPositionY()));
                Cell cell(p);
                cell.SetNoCreate();


                Trinity::AllGameObjectsWithEntryInRange check(caster, GAMEOBJECT_PLATFORM, 300.f);
                Trinity::GameObjectListSearcher<Trinity::AllGameObjectsWithEntryInRange> searcher(caster, gobList, check);

                TypeContainerVisitor<Trinity::GameObjectListSearcher<Trinity::AllGameObjectsWithEntryInRange>, GridTypeMapContainer >  gridGobjectSearcher(searcher);

                cell.Visit(p, gridGobjectSearcher, *caster->GetMap(), *caster, 300.f);

                for (std::list<GameObject*>::const_iterator iter = gobList.begin(); iter != gobList.end(); iter++)
                {
                    if (dist > caster->GetExactDist2d(*iter))
                    {
                        dist = caster->GetExactDist2d(*iter);
                        gobj = *iter;
                    }
                }

                if (gobj)
                {
                    gobj->DestroyForNearbyPlayers();
                    gobj->Use(caster);
                    gobj->Delete();
                    caster->CastSpell(caster->GetPositionX(), caster->GetPositionY(), caster->GetPositionZ() - 2, 101627, true);
                }
            }

            void Register()
            {
                OnEffectHit += SpellEffectFn(spell_echo_of_baine_pulverize_SpellScript::HandleAfterJump, EFFECT_0, SPELL_EFFECT_JUMP_DEST);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_echo_of_baine_pulverize_SpellScript();
        }
};

class npc_baine_totem : public CreatureScript
{
public:
    npc_baine_totem() : CreatureScript("npc_baine_totem") {}

    struct npc_baine_totemAI : public ScriptedAI
    {
        npc_baine_totemAI(Creature *c) : ScriptedAI(c)
        {
        }

        void Reset()
        {
            me->AddAura(SPELL_BAINE_TOTEM, me);
            me->SetFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_SPELLCLICK);
        }

        void OnSpellClick(Unit* clicker,bool& result)
        {
            me->DespawnOrUnsummon();
        }
    };

    CreatureAI *GetAI(Creature *creature) const
    {
        return new npc_baine_totemAI(creature);
    }
};

class spell_echo_of_baine_throw_totem : public SpellScriptLoader
{
    public:
        spell_echo_of_baine_throw_totem() : SpellScriptLoader("spell_echo_of_baine_throw_totem") { }

        class spell_echo_of_baine_throw_totem_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_echo_of_baine_throw_totem_SpellScript);

            void HandleCast()
            {
                GetCaster()->RemoveAurasDueToSpell(101601);
            }

            void Register()
            {
                OnCast += SpellCastFn(spell_echo_of_baine_throw_totem_SpellScript::HandleCast);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_echo_of_baine_throw_totem_SpellScript();
        }
};

void AddSC_boss_echo_of_baine()
{
    new boss_echo_of_baine();
    new spell_echo_of_baine_pulverize();
    new npc_baine_totem();
    new spell_echo_of_baine_throw_totem();
}
