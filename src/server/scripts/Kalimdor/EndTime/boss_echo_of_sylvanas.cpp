/*
 * Copyright (C) 2013-2014 Sovak <sovak007@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "end_time.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"

#define GHOUL_DIST_FROM_CASTER 25

enum Spells
{
    SPELL_BLIGHTED_ARROWS_SUMMON            = 101547,
    SPELL_BLIGHTED_ARROWS_FIRE              = 100763,
    SPELL_BLIGHTED_ARROWS_TRIGGER           = 101552,
    SPELL_BLIGHTED_ARROWS_VISUAL            = 101401,
    SPELL_BLIGHTED_ARROWS_TRIGGEREDBY       = 101567,
    SPELL_TELEPORT                          = 101398,
    SPELL_CALLING_OF_THE_HIGHBORNE          = 100686,
    SPELL_DEATH_GRIP                        = 101397,
    SPELL_SUMMON_GHOULS                     = 101198,
    SPELL_SUMMON_GHOUL                      = 102607,
    SPELL_GHOUL_LINK                        = 100865,
    SPELL_CALLING_OF_THE_HIGHBORNE_VISUAL   = 105766,
    SPELL_WRACKING_PAIN_LINK_DAMAGE         = 101221,
    SPELL_SACRIFICE                         = 101348,
    SPELL_UNHOLY_SHOT                       = 101411,
    SPELL_SHRIEK_OF_THE_HIGHBORNE           = 101412,
    SPELL_BLACK_ARROW                       = 101404,
    SPELL_CALLING_OF_THE_HIGHBORNE_SUMMON   = 100867
};

enum Events
{
    EVENT_BLIGHTED_ARROWS                   = 1,
    EVENT_BLIGHTED_ARROWS_JUMP              = 2,
    EVENT_BLIGHTED_ARROWS_FIRE              = 3,
    EVENT_CALLING_OF_THE_HIGHBORNE          = 4,
    EVENT_CALLING_OF_THE_HIGHBORNE_DELAY    = 5,
    EVENT_CALLING_OF_THE_HIGHBORNE_GHOULS   = 6,
    EVENT_CALLING_OF_THE_HIGHBORNE_END      = 7,
    EVENT_UNHOLY_SHOT                       = 8,
    EVENT_SHRIEK_OF_THE_HIGHBORNE           = 9,
    EVENT_BLACK_ARROW                       = 10
};

enum Npcs
{
    NPC_BLIGHTED_ARROWS         = 54403,
    NPC_ECHO_OF_SYLVANAS        = 54123,
    NPC_GHOUL                   = 54197,
    NPC_FAKE_GHOUL              = 150000
};

enum CreatureText
{
    SAY_AGGRO   = 0,
    SAY_SPELL   = 1,
    SAY_SLAY    = 2,
    SAY_WIPE    = 3,
    SAY_DEATH   = 4
};

class GhoulFilter
{
    public:
        GhoulFilter()
        {
        }

        bool operator()(WorldObject* target)
        {
            if (Creature* creature = target->ToCreature())
                if (creature->GetEntry() == NPC_GHOUL)
                    return false;
            return true;
        }
};

class NoArrowFilter
{
    public:
        NoArrowFilter()
        {
        }

        bool operator()(WorldObject* target)
        {
            if (Creature* creature = target->ToCreature())
                if (creature->GetEntry() == NPC_BLIGHTED_ARROWS)
                    return false;
            return true;
        }
};

class PlayerFilter
{
    public:
        PlayerFilter()
        {
        }

        bool operator()(WorldObject* target)
        {
            if (target->ToPlayer())
                    return false;
            return true;
        }
};

class boss_echo_of_sylvanas : public CreatureScript
{
public:
    boss_echo_of_sylvanas() : CreatureScript("boss_echo_of_sylvanas") 
    {
    }

    BossAI* GetAI(Creature* creature) const
    {
        return new boss_echo_of_sylvanasAI(creature);
    }

    struct boss_echo_of_sylvanasAI : public BossAI
    {
        boss_echo_of_sylvanasAI(Creature* creature) : BossAI(creature, DATA_ECHO_OF_SYLVANAS)
        {
            instance = me->GetInstanceScript();
        }

        InstanceScript* instance;
        std::vector<float> orientation;
        bool saidEvade;

        void Reset()
        {
            for (int i = 0; i < 8; i++)
                instance->SetData64((DATA_GHOUL1 + i), 0);

            events.Reset();
            me->RemoveAllAuras();
            me->RemoveUnitMovementFlag(MOVEMENTFLAG_FLYING);
            me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
            me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PACIFIED);
            me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STUNNED);
            me->SetReactState(REACT_AGGRESSIVE);
            orientation.empty();
            events.ScheduleEvent(EVENT_BLIGHTED_ARROWS, 10000);
            events.ScheduleEvent(EVENT_CALLING_OF_THE_HIGHBORNE, 45000);
        }

        void EnterEvadeMode()
        {
            if (instance)
                instance->SetBossState(DATA_ECHO_OF_SYLVANAS, FAIL);

            me->GetMotionMaster()->MoveTargetedHome();
            Reset();
        }

        void JustReachedHome()
        {
            if (!saidEvade)
            {
                Talk(SAY_WIPE);
                saidEvade = true;
            }
        }

        void EnterCombat(Unit* who) 
        {
            saidEvade = false;
            Talk(SAY_AGGRO);

            if (instance)
                instance->SetBossState(DATA_ECHO_OF_SYLVANAS, IN_PROGRESS);
        }

        void JustDied(Unit* killer)
        {
            Talk(SAY_DEATH);

            if (instance)
                instance->SetBossState(DATA_ECHO_OF_SYLVANAS, DONE);
        }

        void KilledUnit(Unit* victim)
        {
            Talk(SAY_SLAY);
        }

        void CheckPolygon()
        {
            std::list<Player*> playerList;
            Trinity::AnyPlayerInObjectRangeCheck checker(me, VISIBLE_RANGE);
            Trinity::PlayerListSearcher<Trinity::AnyPlayerInObjectRangeCheck> searcher(me, playerList, checker);
            me->VisitNearbyWorldObject(VISIBLE_RANGE, searcher);

            for (std::list<Player*>::iterator iter = playerList.begin(); iter != playerList.end(); iter++)
            {
                bool isWithinTriangle = false;
                std::vector<std::pair<float, float> > cords;

                if (Player* player = (*iter)->ToPlayer())
                {
                    std::list<int32> ghouls = GetDeadGhouls();
                    for (std::list<int32>::const_iterator itr = ghouls.begin(); itr != ghouls.end(); itr++)
                    {
                        if (IsWithinTrangle(player->GetPositionX(), player->GetPositionY(), orientation[*itr]))
                        {
                            isWithinTriangle = true;
                        }
                    }

                    std::vector<std::pair<float, float> > cords;
                    for (int i = 0; i < 8; i++)
                    {
                        if (IsGhoulAlive(i))
                            cords.push_back(std::make_pair(GetGhoul(i)->GetPositionX(), GetGhoul(i)->GetPositionY()));
                        else
                            {
                                float x = me->GetPositionX() + (GetDistanceOfGhouls() * cos(orientation[i]));//Position::NormalizeOrientation(M_PI * 2.f / 8.f) * (i + 1.0f)));
                                float y = me->GetPositionY() + (GetDistanceOfGhouls() * sin(orientation[i]));//Position::NormalizeOrientation(M_PI * 2.f / 8.f) * (i + 1.0f)));
                                cords.push_back(std::make_pair(x, y));
                            }
                    }
                        if (!IsWithinPolygon(cords, player->GetPositionX(), player->GetPositionY()) && !isWithinTriangle)
                            player->CastSpell(player, SPELL_WRACKING_PAIN_LINK_DAMAGE, true);
                }
            }
        }

        bool IsWithinPolygon(std::vector<std::pair<float, float> > polygon, float x, float y)
        {
            float x1, x2;
            int crs = 0;

            for (uint32 i = 0; i < polygon.size(); i++)
            {
                if (polygon[i].first < polygon[(i + 1) % polygon.size()].first)
                {
                    x1 = polygon[i].first;
                    x2 = polygon[(i + 1) % polygon.size()].first;
                } 
                else
                {
                    x1 = polygon[(i + 1) % polygon.size()].first;
                    x2 = polygon[i].first;
                }

                if (x > x1 && x <= x2 && ( y < polygon[i].second || y <= polygon[(i + 1) % polygon.size()].second)) 
                {

                    float dx = polygon[(i + 1) % polygon.size()].first - polygon[i].first;
                    float dy = polygon[(i + 1) % polygon.size()].second - polygon[i].second;
                    float k;
 
                    if (fabs(dx) < 0.000001)
                        k = 0xFFFFFFFF;
                    else
                        k = dy/dx;

                    float m = polygon[i].second - k * polygon[i].first;
               
                    float y2 = k * x + m;
                    if (y <= y2)
                        crs++;
                }
            }
            return (crs % 2 == 1);
        }

        bool IsWithinTrangle(float x, float y, float orientation)
        {
            float dist = VISIBLE_RANGE;
            float amplitude = M_PI * 2.f / 8.f;
            float minOrientation = (orientation - amplitude);
            float maxOrientation = (orientation + amplitude);

            float minX = me->GetPositionX() + (dist * cos(minOrientation));
            float maxX = me->GetPositionX() + (dist * cos(maxOrientation));
            float minY = me->GetPositionY() + (dist * sin(minOrientation));
            float maxY = me->GetPositionY() + (dist * sin(maxOrientation));

            std::vector<std::pair<float, float> > cords;
            cords.push_back(std::make_pair(me->GetPositionX(), me->GetPositionY()));
            cords.push_back(std::make_pair(minX, minY));
            cords.push_back(std::make_pair(maxX, maxY));

            return IsWithinPolygon(cords, x, y);
        }

        void KillGhouls()
        {
            for (int32 i = 0; i < 8; i++)
            {
                if (Unit* ghoul = GetGhoul(i))
                {
                    if (ghoul->IsAlive() && ghoul->IsInWorld())
                    {
                        ghoul->ToCreature()->DespawnOrUnsummon();
                    }
                }
            }
        }

        Unit* GetGhoul(int32 number)
        {
            return sObjectAccessor->GetCreature(*me, instance->GetData64(DATA_GHOUL1 + number));
        }

        float GetDistanceOfGhouls()
        {
            for (int32 i = 0; i < 8; i++)
            {
                if (Unit* ghoul = GetGhoul(i))
                {
                    if (ghoul->IsAlive())
                    {
                        return me->GetExactDist2d(ghoul);
                    }
                }
            }
            return 0.f;
        }

        std::list<int32> GetDeadGhouls()
        {
            std::list<int32> list;
            for (int i = 0; i < 8; i++)
            {
                if (!IsGhoulAlive(i))
                    list.push_back(i);
            }
            return list;
        }

        bool IsGhoulAlive(int32 number)
        {
            return sObjectAccessor->GetCreature(*me, instance->GetData64(DATA_GHOUL1 + number)) ? sObjectAccessor->GetCreature(*me, instance->GetData64(DATA_GHOUL1 + number))->IsAlive() : false;
        }

        void UpdateAI(uint32 diff)
        {
            if (!UpdateVictim())
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_BLIGHTED_ARROWS:
                    {
                        Unit* target = me->GetVictim();

                        if (!target)
                            break;

                        float angle = me->GetAngle(target->GetPositionX(), target->GetPositionY());

                        for (int dist = 2; dist <= 22; dist += 5)
                        {
                            float x = target->GetPositionX() + (dist * cos(angle));
                            float y = target->GetPositionY() + (dist * sin(angle));
                            float z = target->GetPositionZ();
                            me->UpdateGroundPositionZ(x, y, z);

                            me->CastSpell(x, y, z, SPELL_BLIGHTED_ARROWS_SUMMON, true);
                        }
                        events.ScheduleEvent(EVENT_BLIGHTED_ARROWS_JUMP, 1000);
                        break;
                    }
                    case EVENT_BLIGHTED_ARROWS_JUMP:
                    {
                        me->AddUnitMovementFlag(MOVEMENTFLAG_FLYING);
                        Position pos;
                        me->GetPosition(&pos);
                        pos.m_positionZ += 8.f;
                        me->GetMotionMaster()->MoveJump(pos, 8, 8);
                        me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
                        events.ScheduleEvent(EVENT_BLIGHTED_ARROWS_FIRE, 1000);
                        break;
                    }
                    case EVENT_BLIGHTED_ARROWS_FIRE:
                    {
                        me->CastSpell(me, SPELL_BLIGHTED_ARROWS_VISUAL, true);

                        me->RemoveUnitMovementFlag(MOVEMENTFLAG_FLYING);
                        me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_DISABLE_MOVE);
                        events.ScheduleEvent(EVENT_UNHOLY_SHOT, 8000);
                        break;
                    }
                    case EVENT_UNHOLY_SHOT:
                    {
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, VISIBLE_RANGE, true))
                            DoCast(target, SPELL_UNHOLY_SHOT, false);
                        events.ScheduleEvent(EVENT_SHRIEK_OF_THE_HIGHBORNE, 9000);
                        break;
                    }
                    case EVENT_SHRIEK_OF_THE_HIGHBORNE:
                    {
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, VISIBLE_RANGE, true))
                            DoCast(target, SPELL_SHRIEK_OF_THE_HIGHBORNE, false);
                        events.ScheduleEvent(EVENT_BLACK_ARROW, 10000);
                        break;
                    }
                    case EVENT_BLACK_ARROW:
                    {
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, VISIBLE_RANGE, true))
                            DoCast(target, SPELL_BLACK_ARROW, false);
                        break;
                    }
                    case EVENT_CALLING_OF_THE_HIGHBORNE:
                    {
                        me->CastSpell(me, SPELL_TELEPORT, true);
                        if (Unit* victim = me->GetVictim())
                            me->SetOrientation(me->GetAngle(victim->GetPositionX(), victim->GetPositionY()));
                        me->SetReactState(REACT_PASSIVE);
                        me->SetTarget(me->GetGUID());
                        me->GetMotionMaster()->Clear(true);
                        me->CastSpell(me, SPELL_CALLING_OF_THE_HIGHBORNE, true);
                        events.ScheduleEvent(EVENT_CALLING_OF_THE_HIGHBORNE_DELAY, 4000);
                        Talk(SAY_SPELL);
                        break;
                    }
                    case EVENT_CALLING_OF_THE_HIGHBORNE_DELAY:
                    {
                        me->CastSpell(me, SPELL_DEATH_GRIP, true);
                        me->CastSpell(me, SPELL_SUMMON_GHOULS, true);
                        events.ScheduleEvent(EVENT_CALLING_OF_THE_HIGHBORNE_GHOULS, 2000);
                        break;
                    }
                    case EVENT_CALLING_OF_THE_HIGHBORNE_GHOULS:
                    {
                        CheckPolygon();

                        if (!orientation.size())
                            for (int32 j = 0; j < 8; j ++)
                                if (Unit* unit = sObjectAccessor->GetCreature(*me, instance->GetData64(DATA_GHOUL1 + j)))
                                    orientation.push_back(me->GetAngle(unit));

                        if (GetDistanceOfGhouls() < 2.0f)
                        {
                            DoCast(SPELL_SACRIFICE);
                            KillGhouls();
                            events.ScheduleEvent(EVENT_CALLING_OF_THE_HIGHBORNE_END, 500);

                        }
                        else
                            events.ScheduleEvent(EVENT_CALLING_OF_THE_HIGHBORNE_GHOULS, 500);
                        break;
                    }
                    case EVENT_CALLING_OF_THE_HIGHBORNE_END:
                    {
                        for (int i = 0; i < 8; i++)
                            instance->SetData64((DATA_GHOUL1 + i), 0);

                        me->SetReactState(REACT_AGGRESSIVE);
                        me->RemoveAllAuras();
                        orientation.empty();

                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, VISIBLE_RANGE, true))
                        {
                            me->SetTarget(target->GetGUID());
                            me->GetMotionMaster()->MoveChase(target);
                        }

                        events.ScheduleEvent(EVENT_BLIGHTED_ARROWS, 10000);
                        events.ScheduleEvent(EVENT_CALLING_OF_THE_HIGHBORNE, 45000);
                        break;
                    }
                }
            }

            if (!me->HasAura(SPELL_CALLING_OF_THE_HIGHBORNE))
                DoMeleeAttackIfReady();
        }
    };
};

class npc_blighted_arrow : public CreatureScript
{
public:
    npc_blighted_arrow() : CreatureScript("npc_blighted_arrow") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_blighted_arrowAI (creature);
    }

    struct npc_blighted_arrowAI : public ScriptedAI
    {
        npc_blighted_arrowAI(Creature* creature) : ScriptedAI(creature) {}

        void Reset()
        {
            me->AddAura(SPELL_BLIGHTED_ARROWS_TRIGGER, me);
            me->AddUnitState(UNIT_STATE_ROOT);
        }
    };
};

class spell_echo_of_sylvanas_blighted_arrows_visual : public SpellScriptLoader
{
    public:
        spell_echo_of_sylvanas_blighted_arrows_visual() : SpellScriptLoader("spell_echo_of_sylvanas_blighted_arrows_visual") { }

        class spell_echo_of_sylvanas_blighted_arrows_visual_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_echo_of_sylvanas_blighted_arrows_visual_SpellScript);

            void HandleBoom(SpellEffIndex effIndex)
            {
                Unit* caster = GetCaster();

                if (!caster->ToCreature())
                    return;

                CellCoord p(Trinity::ComputeCellCoord(caster->GetPositionX(), caster->GetPositionY()));
                Cell cell(p);
                cell.SetNoCreate();

                std::list<Creature*> unitList;

                Trinity::AllCreaturesOfEntryInRange check(caster, NPC_BLIGHTED_ARROWS, VISIBLE_RANGE);
                Trinity::CreatureListSearcher<Trinity::AllCreaturesOfEntryInRange> searcher(caster, unitList, check);

                TypeContainerVisitor<Trinity::CreatureListSearcher<Trinity::AllCreaturesOfEntryInRange>, GridTypeMapContainer >  gridGobjectSearcher(searcher);

                cell.Visit(p, gridGobjectSearcher, *caster->GetMap(), *caster, VISIBLE_RANGE);

                for (std::list<Creature*>::const_iterator iter = unitList.begin(); iter != unitList.end(); iter++)
                {
                    if (Creature* target = *iter)
                    {
                        if (target->HasAura(SPELL_BLIGHTED_ARROWS_TRIGGER))
                        {
                            target->CastSpell(target, SPELL_BLIGHTED_ARROWS_FIRE, true);
                            target->RemoveAurasDueToSpell(SPELL_BLIGHTED_ARROWS_TRIGGER);
                        }
                    }
                }
            }

            void SelectTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(NoArrowFilter());
            }

            void Register()
            {
                OnEffectHit += SpellEffectFn(spell_echo_of_sylvanas_blighted_arrows_visual_SpellScript::HandleBoom, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_echo_of_sylvanas_blighted_arrows_visual_SpellScript::SelectTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENTRY);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_echo_of_sylvanas_blighted_arrows_visual_SpellScript();
        }
};

class spell_echo_of_sylvanas_death_grip : public SpellScriptLoader
{
    public:
        spell_echo_of_sylvanas_death_grip() : SpellScriptLoader("spell_echo_of_sylvanas_death_grip") { }

        class spell_echo_of_sylvanas_death_grip_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_echo_of_sylvanas_death_grip_SpellScript);

            void SummonPlayers(SpellEffIndex effIndex)
            {
                if (Player* player = GetHitUnit()->ToPlayer())
                {
                    player->CastSpell(GetCaster(), GetSpellInfo()->Effects[effIndex].TriggerSpell, true);
                }
            }

            void SelectTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(PlayerFilter());
            }

            void Register()
            {
                OnEffectHitTarget += SpellEffectFn(spell_echo_of_sylvanas_death_grip_SpellScript::SummonPlayers, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_echo_of_sylvanas_death_grip_SpellScript::SelectTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_echo_of_sylvanas_death_grip_SpellScript::SelectTargets, EFFECT_1, TARGET_UNIT_SRC_AREA_ENEMY);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_echo_of_sylvanas_death_grip_SpellScript::SelectTargets, EFFECT_2, TARGET_UNIT_SRC_AREA_ENEMY);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_echo_of_sylvanas_death_grip_SpellScript();
        }
};


class spell_echo_of_sylvanas_summon_ghouls : public SpellScriptLoader
{
    public:
        spell_echo_of_sylvanas_summon_ghouls() : SpellScriptLoader("spell_echo_of_sylvanas_summon_ghouls") { }

        class spell_echo_of_sylvanas_summon_ghouls_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_echo_of_sylvanas_summon_ghouls_SpellScript);

            void SummonGhouls(SpellEffIndex effIndex)
            {
                if (Unit* caster = GetCaster())
                {
                    for (int i = 1; i <= 8; i++)
                    {
                        if (InstanceScript* instance = caster->GetInstanceScript())
                        {
                            float angle = M_PI * 2.f / 8.f * i;
                            float x = caster->GetPositionX() + (cos(angle) * GHOUL_DIST_FROM_CASTER);
                            float y = caster->GetPositionY() + (sin(angle) * GHOUL_DIST_FROM_CASTER);
                            float z = caster->GetPositionZ();
                            caster->UpdateGroundPositionZ(x, y, z);
                            z += 2.f; // Security reasons
                            GetCaster()->CastSpell(x, y, z, SPELL_SUMMON_GHOUL, true);

                            std::list<Creature*> unitList;
                            CellCoord p(Trinity::ComputeCellCoord(caster->GetPositionX(), caster->GetPositionY()));
                            Cell cell(p);
                            cell.SetNoCreate();

                            Trinity::AllCreaturesOfEntryInRange check(caster, NPC_GHOUL, VISIBLE_RANGE);
                            Trinity::CreatureListSearcher<Trinity::AllCreaturesOfEntryInRange> searcher(caster, unitList, check);

                            TypeContainerVisitor<Trinity::CreatureListSearcher<Trinity::AllCreaturesOfEntryInRange>, GridTypeMapContainer >  gridGobjectSearcher(searcher);

                            cell.Visit(p, gridGobjectSearcher, *caster->GetMap(), *caster, VISIBLE_RANGE);

                            for (std::list<Creature*>::const_iterator iter = unitList.begin(); iter != unitList.end(); iter++)
                            {
                                bool found = false;
                                for (int j = 0; j < 8; j++)
                                {
                                    if ((*iter)->GetGUID() == instance->GetData64(DATA_GHOUL1 + j))
                                        found = true;

                                    if (!found && !instance->GetData64(DATA_GHOUL1 + j))
                                    {
                                        instance->SetData64(DATA_GHOUL1 + j, (*iter)->GetGUID());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            void Register()
            {
                OnEffectHit += SpellEffectFn(spell_echo_of_sylvanas_summon_ghouls_SpellScript::SummonGhouls, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_echo_of_sylvanas_summon_ghouls_SpellScript();
        }
};

class npc_ugly_ghoul : public CreatureScript
{
public:
    npc_ugly_ghoul() : CreatureScript("npc_ugly_ghoul") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_ugly_ghoulAI (creature);
    }

    struct npc_ugly_ghoulAI : public ScriptedAI
    {
        npc_ugly_ghoulAI(Creature* creature) : ScriptedAI(creature)
        {
            instance = me->GetInstanceScript();
        }

        uint32 timer;
        InstanceScript const* instance;
        int32 myNumber;

        void Reset()
        {
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PACIFIED);
            me->SetReactState(REACT_PASSIVE);
            timer = 1500;
            me->SetSpeed(MOVE_WALK, 0.20, true);
            me->SetSpeed(MOVE_RUN, 0.20, true);
            Unit* sylvanas = me->FindNearestCreature(NPC_ECHO_OF_SYLVANAS, VISIBLE_RANGE, true);
            me->SummonCreature(NPC_FAKE_GHOUL, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), me->GetAngle(sylvanas->GetPositionX(), sylvanas->GetPositionY()));
        }

        void JustDied(Unit* killer)
        {
            me->DespawnOrUnsummon();
        }

        void UpdateAI(uint32 diff)
        {
            if (!instance)
                return;

            Unit* sylvanas = me->FindNearestCreature(NPC_ECHO_OF_SYLVANAS, VISIBLE_RANGE, true);

            // Wait for other friends
            if (timer <= diff)
            {
                for (int i = 0; i < 8; i++)
                    if (instance->GetData64(DATA_GHOUL1 + i) == me->GetGUID())
                        myNumber = i;

                Unit* ghoul = sObjectAccessor->GetCreature(*me, instance->GetData64(DATA_GHOUL1 + (myNumber != 7 ? myNumber + 1 : 0)));
                if (ghoul)
                {
                    me->AddAura(SPELL_GHOUL_LINK, ghoul);
                    me->SetOrientation(me->GetAngle(sylvanas->GetPositionX(), sylvanas->GetPositionY()));
                    //me->CastSpell(me, SPELL_CALLING_OF_THE_HIGHBORNE_VISUAL, true);

                    if (!sylvanas)
                        return;

                    me->SetOrientation(me->GetAngle(sylvanas->GetPositionX(), sylvanas->GetPositionY()));
                    me->GetMotionMaster()->MovePoint(0, sylvanas->GetPositionX(), sylvanas->GetPositionY(), sylvanas->GetPositionZ(), false);
                }

                timer = -1;
            }
            else
                timer -= diff;

            if (sylvanas)
            {
                me->SetOrientation(me->GetAngle(sylvanas->GetPositionX(), sylvanas->GetPositionY()));
            }
        }
    };
};

class npc_fake_ghoul : public CreatureScript
{
public:
    npc_fake_ghoul() : CreatureScript("npc_fake_ghoul") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_fake_ghoulAI (creature);
    }

    struct npc_fake_ghoulAI : public ScriptedAI
    {
        npc_fake_ghoulAI(Creature* creature) : ScriptedAI(creature)
        {
            instance = me->GetInstanceScript();
        }

        uint32 timer;
        uint32 timer2;
        InstanceScript const* instance;
        int32 myNumber;
        Unit* defGhoul;

        void Reset()
        {
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PACIFIED);
            me->SetReactState(REACT_PASSIVE);
            timer2 = 3000;
            me->SetSpeed(MOVE_WALK, 0.20, true);
            me->SetSpeed(MOVE_RUN, 0.20, true);
            me->CastSpell(me, SPELL_CALLING_OF_THE_HIGHBORNE_SUMMON, true);
            defGhoul = NULL;
            me->AddAura(SPELL_CALLING_OF_THE_HIGHBORNE_VISUAL, me);
        }

        void JustDied(Unit* killer)
        {
            me->DespawnOrUnsummon();
        }

        void UpdateAI(uint32 diff)
        {
            if (!instance)
                return;

            if (timer2 <= diff)
            {
                me->RemoveAurasDueToSpell(SPELL_CALLING_OF_THE_HIGHBORNE_SUMMON);
                timer2 = -1;
            }
            else
                timer2 -= diff;

            Unit* ghoul = me->FindNearestCreature(NPC_GHOUL, VISIBLE_RANGE, true);

            if (!defGhoul)
                defGhoul = ghoul;

            if (!ghoul && (timer2 <= diff || timer2 > 10000))
                me->DespawnOrUnsummon();
				
				{
             me->DespawnOrUnsummon();
             return;
                }

            Unit* sylvanas = me->FindNearestCreature(NPC_ECHO_OF_SYLVANAS, VISIBLE_RANGE, true);
            if (!sylvanas)
                return;

            me->SetFloatValue(OBJECT_FIELD_SCALE_X, (me->GetDistance(sylvanas) / 30.f) + 0.10f);


            if (defGhoul->GetDistance(me) > 10 || defGhoul->isDead())
                me->DespawnOrUnsummon();
				
				{
             me->DespawnOrUnsummon();
             return;
                }

            me->SetOrientation(me->GetAngle(sylvanas->GetPositionX(), sylvanas->GetPositionY()));
            me->GetMotionMaster()->MovePoint(0, sylvanas->GetPositionX(), sylvanas->GetPositionY(), sylvanas->GetPositionZ(), false);

        }
    };
};

class spell_echo_of_sylvanas_ghoul_link : public SpellScriptLoader
{
    public:
        spell_echo_of_sylvanas_ghoul_link() : SpellScriptLoader("spell_echo_of_sylvanas_ghoul_link") { }

        class spell_echo_of_sylvanas_ghoul_link_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_echo_of_sylvanas_ghoul_link_SpellScript);

            void SelectTargets(std::list<WorldObject*>& targets)
            {
                targets.remove_if(GhoulFilter());
            }

            void Register()
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_echo_of_sylvanas_ghoul_link_SpellScript::SelectTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_echo_of_sylvanas_ghoul_link_SpellScript();
        }
};

void AddSC_boss_echo_of_sylvanas()
{
    new boss_echo_of_sylvanas();
    new npc_blighted_arrow();
    new spell_echo_of_sylvanas_blighted_arrows_visual();
    new spell_echo_of_sylvanas_death_grip();
    new spell_echo_of_sylvanas_summon_ghouls();
    new npc_ugly_ghoul();
    new spell_echo_of_sylvanas_ghoul_link();
    new npc_fake_ghoul();
}
