/*
 * Copyright (C) 2013-2014 Sovak <sovak007@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptedGossip.h"
#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "end_time.h"
#include "Spell.h"
#include "GameObjectAI.h"

enum InstanceTeleporeter
{
    START_TELEPORT          = 102564,
    JAINA_TELEPORT          = 102126,
    BAINE_TELEPORT          = 103868,
    SYLVANAS_TELEPORT       = 102579,
    TYRANDE_TELEPORT        = 104761,
    MUROZOND_TELEPORT       = 104764,
};

class end_time_teleporter : public GameObjectScript
{
    public:
        end_time_teleporter() : GameObjectScript("end_time_teleporter") { }

        struct end_time_teleporterAI : public GameObjectAI
        {
            end_time_teleporterAI(GameObject* go) : GameObjectAI(go)
            {
            }
            
            bool GossipHello(Player* player)
            {
                player->PlayerTalkClass->ClearMenus();
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Teleport to Entryway of Time.", GOSSIP_SENDER_MAIN, START_TELEPORT);

                if (InstanceScript* instance = go->GetInstanceScript())
                {
                    switch (instance->GetData64(DATA_BOSS1))
                    {
                        case 0:
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Teleport to Obsidian Dragonshire.", GOSSIP_SENDER_MAIN, BAINE_TELEPORT);
                            break;
                        case 1:
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Teleport to Azure Dragonshire.", GOSSIP_SENDER_MAIN, JAINA_TELEPORT);
                            break;                
                   }
                    
                    if (instance->GetBossState(instance->GetData64(DATA_BOSS1)) == DONE)
                    {
                        switch (instance->GetData64(DATA_BOSS2))
                        {
                            case 2:            
                                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Teleport to Ruby Dragonshire.", GOSSIP_SENDER_MAIN, SYLVANAS_TELEPORT);
                                break;
                            case 3:
                                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Teleport to Emeral Dragonshire.", GOSSIP_SENDER_MAIN, TYRANDE_TELEPORT);
                                break;
                        }
                    }
            
                    if (instance->GetBossState(instance->GetData64(DATA_BOSS1)) == DONE && instance->GetBossState(instance->GetData64(DATA_BOSS2)) == DONE)
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Teleport to Bronze Dragonshire.", GOSSIP_SENDER_MAIN, MUROZOND_TELEPORT);
                }
            
                player->SEND_GOSSIP_MENU(player->GetGossipTextId(go), go->GetGUID());
                return true;
            }

            void UpdateAI(uint32 diff)
            {
            }
        };

        bool OnGossipSelect(Player* player, GameObject* go, uint32 sender, uint32 action) 
        {
            if (!player->getAttackers().empty())
                return false;

            player->CastSpell(player, action, true);
            player->CLOSE_GOSSIP_MENU();

            return true;
        }

        GameObjectAI* GetAI(GameObject* go) const
        {
            return new end_time_teleporterAI(go);
        }
};

void AddSC_end_time_teleporter()
{
    new end_time_teleporter;
}