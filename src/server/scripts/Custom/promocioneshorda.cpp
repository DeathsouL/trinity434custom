/* Creado para BoT WOW
NPC PVE
Por luis y bot
 */

#include "ScriptPCH.h"

class npc_promohorda : public CreatureScript 

{
public:
    npc_promohorda() : CreatureScript("npc_promohorda") { } 
 
    bool OnGossipHello(Player* player, Creature* creature) OVERRIDE 
    {
        player->ADD_GOSSIP_ITEM(7, "Escoge tu promocion: ", GOSSIP_SENDER_MAIN, 99);
		switch (player->getClass())
		{
				case CLASS_DRUID: player->ADD_GOSSIP_ITEM(10, "Promocion de Druida Equilibrio", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+30); player->ADD_GOSSIP_ITEM(10, "Promocion de Druida Feral", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+44); player->ADD_GOSSIP_ITEM(10, "Promocion de Druida Restauracion", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+45); break;
				case CLASS_SHAMAN: player->ADD_GOSSIP_ITEM(10, "Promocion de Chaman Elemental", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+31); player->ADD_GOSSIP_ITEM(10, "Promocion de Chaman Mejora", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+46); player->ADD_GOSSIP_ITEM(10, "Promocion de Chaman Restauracion", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+47); break;
				case CLASS_PALADIN: player->ADD_GOSSIP_ITEM(10, "Promocion de Paladin Represion", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+32); player->ADD_GOSSIP_ITEM(10, "Promocion de Paladin Sagrado", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+40); player->ADD_GOSSIP_ITEM(10, "Promocion de Paladin Tanque", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+51); break;
				case CLASS_WARRIOR: player->ADD_GOSSIP_ITEM(10, "Promocion de Guerrero DPS", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+33); player->ADD_GOSSIP_ITEM(10, "Promocion de Guerrero Tanque", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+50); break;
				case CLASS_PRIEST: player->ADD_GOSSIP_ITEM(10, "Promocion de Sacerdote Sombras", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+34); player->ADD_GOSSIP_ITEM(10, "Promocion de Sacerdote Sagrado o Disciplina", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+42); break;
				case CLASS_DEATH_KNIGHT: player->ADD_GOSSIP_ITEM(10, "Promocion de DK TANQUE", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+35); player->ADD_GOSSIP_ITEM(10, "Promocion de DK DPS", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+49); break;
				case CLASS_ROGUE: player->ADD_GOSSIP_ITEM(10, "Promocion de Picaro", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+36); break;
				case CLASS_HUNTER: player->ADD_GOSSIP_ITEM(10, "Promocion de Cazador", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+37); break;
				case CLASS_MAGE: player->ADD_GOSSIP_ITEM(10, "Promocion de Mago", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+38); break;
				case CLASS_WARLOCK: player->ADD_GOSSIP_ITEM(10, "Promocion de Brujo", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+39); break;
		}
                player->SEND_GOSSIP_MENU(1, creature->GetGUID()); 
                return true;
    }
 
    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 actions) OVERRIDE
    {
	    
		if (player->getLevel() == 85) 
		{
            player->GetSession()->SendAreaTriggerMessage("No puedes recibir la promocion mas de una vez por personaje.");
			player->CLOSE_GOSSIP_MENU();
				return true;
        }

		if (player->getLevel() == 1) 
		{
            player->GetSession()->SendAreaTriggerMessage("Debes obtener primero la promocion pvp.");
			player->CLOSE_GOSSIP_MENU();
				return true;
        }
		
        if (player->getLevel() == 50) 
        {
            uint32 accountID = player->GetSession()->GetAccountId();
            QueryResult result = CharacterDatabase.PQuery("SELECT COUNT(`guid`) FROM `characters` WHERE `account`=%u", accountID);
            Field *fields = result->Fetch();
            uint32 personajes = fields[0].GetUInt32();

            if (personajes < 4)			
                    {
					    if (actions == 99)
						{
						    player->CLOSE_GOSSIP_MENU();
						    return false;
						}
						player->GetSession()->SendAreaTriggerMessage("Has recibido tu promocion pvp y pve satisfactoriamente, Enhorabuena.");
						player->GiveLevel(85);
						player->SetMoney(150000000);
						player->AddItem(71665, 1);
						player->AddItem(30609, 1);
						switch(actions)
                              {								  
								  case GOSSIP_ACTION_INFO_DEF+30: // Druida Equilibrio
								  player->AddItem(60282, 1);
								  player->AddItem(59512, 1);
								  player->AddItem(60284, 1);
								  player->AddItem(60281, 1);
								  player->AddItem(63497, 1);
								  player->AddItem(60283, 1);
								  player->AddItem(69586, 1);
								  player->AddItem(59451, 1);
								  player->AddItem(60285, 1);
								  player->AddItem(64904, 1);
								  player->AddItem(58188, 1);
								  player->AddItem(58183, 1);
								  player->AddItem(67131, 1);
							      player->AddItem(59525, 1);
								  player->AddItem(70111, 1);
								  player->AddItem(59519, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+44: // Druida Feral
								  player->AddItem(60286, 1);
								  player->AddItem(59512, 1);
								  player->AddItem(60289, 1);
								  player->AddItem(60287, 1);
								  player->AddItem(59457, 1);
								  player->AddItem(59329, 1);
								  player->AddItem(60285, 1);
								  player->AddItem(59502, 1);
								  player->AddItem(60288, 1);
								  player->AddItem(59469, 1);
								  player->AddItem(69843, 1);
								  player->AddItem(64673, 1);
								  player->AddItem(59519, 1);
								  player->AddItem(59326, 1);
								  player->AddItem(67136, 1);
								  player->AddItem(69602, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+45: // Druida Restauracion
								  player->AddItem(60277, 1);
								  player->AddItem(59483, 1);
								  player->AddItem(59218, 1);
								  player->AddItem(60279, 1);
								  player->AddItem(59516, 1);
								  player->AddItem(60280, 1);
								  player->AddItem(60280, 1);
								  player->AddItem(59321, 1);
								  player->AddItem(60278, 1);
								  player->AddItem(59495, 1);
								  player->AddItem(59525, 1);
								  player->AddItem(64673, 1);
								  player->AddItem(59354, 1);
								  player->AddItem(59500, 1);
								  player->AddItem(59220, 1);
								  player->AddItem(59501, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+31: // Chaman Elemental
								  player->AddItem(60315, 1);
								  player->AddItem(59512, 1);
								  player->AddItem(60317, 1);
								  player->AddItem(58193, 1);
								  player->AddItem(60313, 1);
								  player->AddItem(60211, 1);
								  player->AddItem(63536, 1);
								  player->AddItem(59484, 1);
								  player->AddItem(63487, 1);
								  player->AddItem(60314, 1);
								  player->AddItem(56538, 1);
								  player->AddItem(59336, 1);
								  player->AddItem(59234, 1);
								  player->AddItem(58188, 1);
								  player->AddItem(67129, 1);
								  player->AddItem(59519, 1);
								  player->AddItem(59326, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+46: // Chaman Mejora
								  player->AddItem(60320, 1);
								  player->AddItem(59517, 1);
								  player->AddItem(60322, 1);
								  player->AddItem(59348, 1);
								  player->AddItem(60318, 1);
								  player->AddItem(59355, 1);
								  player->AddItem(59474, 1);
								  player->AddItem(56316, 1);
								  player->AddItem(60319, 1);
								  player->AddItem(56539, 1);
								  player->AddItem(60321, 1);
								  player->AddItem(58199, 1);
								  player->AddItem(59121, 1);
								  player->AddItem(67136, 1);
								  player->AddItem(59473, 2);
								  player->AddItem(59441, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+47: // Chaman Restauracion
								  player->AddItem(60308, 1);
								  player->AddItem(59483, 1);
								  player->AddItem(60311, 1);
								  player->AddItem(59516, 1);
								  player->AddItem(59335, 1);
								  player->AddItem(59310, 1);
								  player->AddItem(59459, 1);
								  player->AddItem(55070, 1);
								  player->AddItem(56350, 1);
								  player->AddItem(60312, 1);
								  player->AddItem(59334, 1);
								  player->AddItem(60310, 1);
								  player->AddItem(59350, 1);
								  player->AddItem(58189, 1);
								  player->AddItem(59220, 1);
								  player->AddItem(59500, 1);
								  player->AddItem(58184, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+32: // Paladin Represion
								  player->AddItem(59344, 1);
								  player->AddItem(59442, 1);
								  player->AddItem(59471, 1);
								  player->AddItem(59316, 1);
								  player->AddItem(59507, 1);
								  player->AddItem(59118, 1);
								  player->AddItem(60345, 1);
								  player->AddItem(59342, 1);
								  player->AddItem(59503, 1);
								  player->AddItem(59464, 1);
								  player->AddItem(63679, 1);
								  player->AddItem(64674, 1);
								  player->AddItem(59224, 1);
								  player->AddItem(59506, 1);
								  player->AddItem(59518, 1);
								  player->AddItem(67139, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+40: // Paladin Healer 
								  player->AddItem(59509, 1);
								  player->AddItem(59483, 1);
								  player->AddItem(59311, 1);
								  player->AddItem(59340, 1);
								  player->AddItem(59516, 1);
								  player->AddItem(59497, 1);
								  player->AddItem(60363, 1);
								  player->AddItem(59450, 1);
								  player->AddItem(60361, 1);
								  player->AddItem(59216, 1);
								  player->AddItem(59459, 1);
								  player->AddItem(59513, 1);
								  player->AddItem(59354, 1);
								  player->AddItem(59500, 1);
								  player->AddItem(59220, 1);
								  player->AddItem(59501, 1);
								  player->AddItem(64673, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+34: // Sacerdote Sombras
								  player->AddItem(59219, 1);
								  player->AddItem(59512, 1);
								  player->AddItem(59325, 1);
								  player->AddItem(59454, 1);
								  player->AddItem(59457, 1);
								  player->AddItem(59475, 1);
								  player->AddItem(59498, 1);
								  player->AddItem(59349, 1);
								  player->AddItem(59336, 1);
								  player->AddItem(59234, 1);
								  player->AddItem(59525, 1);
								  player->AddItem(59460, 1);
								  player->AddItem(59519, 1);
								  player->AddItem(59326, 1);
								  player->AddItem(59501, 1);
								  player->AddItem(69602, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+42: // Sacerdote Sagrado
								  player->AddItem(63534, 1);
								  player->AddItem(59483, 1);
								  player->AddItem(59337, 1);
								  player->AddItem(59468, 1);
								  player->AddItem(59516, 1);
								  player->AddItem(59322, 1);
								  player->AddItem(59313, 1);
								  player->AddItem(59217, 1);
								  player->AddItem(60261, 1);
								  player->AddItem(59508, 1);
								  player->AddItem(59525, 1);
								  player->AddItem(59314, 1);
								  player->AddItem(59354, 1);
								  player->AddItem(59500, 1);
								  player->AddItem(59220, 1);
								  player->AddItem(59501, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+33: // Guerrero Dps
								  player->AddItem(59344, 1);
								  player->AddItem(60670, 1);
								  player->AddItem(59471, 1);
								  player->AddItem(59316, 1);
								  player->AddItem(59342, 1);
								  player->AddItem(60324, 1);
								  player->AddItem(59221, 1);
								  player->AddItem(59118, 1);
								  player->AddItem(59225, 1);
								  player->AddItem(59518, 1);
								  player->AddItem(67139, 1);
								  player->AddItem(59224, 1);
								  player->AddItem(59506, 1);
								  player->AddItem(59507, 1);
								  player->AddItem(63679, 1);
								  player->AddItem(63679, 2);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+35: // DK Escarcha
								  player->AddItem(71780, 1);
								  player->AddItem(63531, 1);
								  player->AddItem(59319, 1);
								  player->AddItem(59471, 1);
								  player->AddItem(59316, 1);
								  player->AddItem(59507, 1);
								  player->AddItem(59118, 1);
								  player->AddItem(59225, 1);
								  player->AddItem(64676, 1);
								  player->AddItem(59117, 1);
								  player->AddItem(59317, 1);
								  player->AddItem(59328, 1);
								  player->AddItem(59224, 1);
								  player->AddItem(59461, 1);
								  player->AddItem(59233, 1);
								  player->AddItem(59518, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+49: // DK Profano
								  player->AddItem(59487, 1);
								  player->AddItem(59442, 1);
								  player->AddItem(59471, 1);
								  player->AddItem(59316, 1);
								  player->AddItem(59507, 1);
								  player->AddItem(59118, 1);
								  player->AddItem(59225, 1);
								  player->AddItem(59117, 1);
								  player->AddItem(59317, 1);
								  player->AddItem(59221, 1);
								  player->AddItem(59333, 1);
								  player->AddItem(59333, 1);
								  player->AddItem(64674, 1);
								  player->AddItem(59224, 1);
								  player->AddItem(59461, 1);
								  player->AddItem(59233, 1);
								  player->AddItem(59518, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+36: // Picaro
								  player->AddItem(60202, 1);
								  player->AddItem(59517, 1);
								  player->AddItem(59120, 1);
								  player->AddItem(67135, 1);
								  player->AddItem(67134, 1);
								  player->AddItem(59329, 1);
								  player->AddItem(60298, 1);
								  player->AddItem(59502, 1);
								  player->AddItem(59343, 1);
								  player->AddItem(59469, 1);
								  player->AddItem(59122, 1);
								  player->AddItem(59122, 1);
								  player->AddItem(63532, 1);
								  player->AddItem(59473, 1);
								  player->AddItem(59520, 1);
								  player->AddItem(59121, 1);
								  player->AddItem(67136, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+37: // Cazador
								  player->AddItem(59504, 1);
								  player->AddItem(59517, 1);
								  player->AddItem(59222, 1);
								  player->AddItem(59119, 1);
								  player->AddItem(67134, 1);
								  player->AddItem(59355, 1);
								  player->AddItem(59472, 1);
								  player->AddItem(59485, 1);
								  player->AddItem(59331, 1);
								  player->AddItem(59315, 1);
								  player->AddItem(59474, 1);
								  player->AddItem(59320, 1);
								  player->AddItem(59441, 1);
								  player->AddItem(59473, 1);
								  player->AddItem(59121, 1);
								  player->AddItem(67136, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+38: // Mago
								  player->AddItem(59219, 1);
								  player->AddItem(59512, 1);
								  player->AddItem(59325, 1);
								  player->AddItem(59454, 1);
								  player->AddItem(59457, 1);
								  player->AddItem(59475, 1);
								  player->AddItem(60247, 1);
								  player->AddItem(59349, 1);
								  player->AddItem(60245, 1);
								  player->AddItem(59234, 1);
								  player->AddItem(59525, 1);
								  player->AddItem(59460, 1);
								  player->AddItem(59519, 1);
								  player->AddItem(59326, 1);
								  player->AddItem(59501, 1);
								  player->AddItem(69602, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+39: // Brujo
								  player->AddItem(59219, 1);
								  player->AddItem(59512, 1);
								  player->AddItem(59325, 1);
								  player->AddItem(59454, 1);
								  player->AddItem(59457, 1);
								  player->AddItem(59475, 1);
								  player->AddItem(60248, 1);
								  player->AddItem(59349, 1);
								  player->AddItem(60250, 1);
								  player->AddItem(59234, 1);
								  player->AddItem(59525, 1);
								  player->AddItem(59460, 1);
								  player->AddItem(59326, 1);
								  player->AddItem(59519, 1);
								  player->AddItem(59501, 1);
								  player->AddItem(69602, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+50: // Guerrero TANQUE
								  player->AddItem(63531, 1);
								  player->AddItem(59442, 1);
								  player->AddItem(59356, 1);
								  player->AddItem(59316, 1);
								  player->AddItem(59466, 1);
								  player->AddItem(59118, 1);
								  player->AddItem(59505, 1);
								  player->AddItem(59117, 1);
								  player->AddItem(59317, 1);
								  player->AddItem(59221, 1);
								  player->AddItem(59347, 1);
								  player->AddItem(59444, 1);
								  player->AddItem(69637, 1);
								  player->AddItem(59461, 1);
								  player->AddItem(59224, 1);
								  player->AddItem(59233, 2);
								  player->AddItem(59518, 2);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+51: // Paladin Tanque 
								  player->AddItem(63531, 1);
								  player->AddItem(59319, 1);
								  player->AddItem(59901, 1);
								  player->AddItem(59486, 1);
								  player->AddItem(59466, 1);
								  player->AddItem(59470, 1);
								  player->AddItem(60355, 1);
								  player->AddItem(59117, 1);
								  player->AddItem(59317, 1);
								  player->AddItem(59328, 1);
								  player->AddItem(59333, 1);
								  player->AddItem(59444, 1);
								  player->AddItem(64676, 1);
								  player->AddItem(59332, 1);
								  player->AddItem(59224, 1);
								  player->AddItem(59233, 1);
								  player->AddItem(59518, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;								  
	                          }
                    }			
			
					if (personajes > 3)
                    {
                        player->GetSession()->SendAreaTriggerMessage("No puedes recibir la promocion teniendo mas de tres personajes.");
						player->CLOSE_GOSSIP_MENU();
						return true;
                    }
			
		    player->CLOSE_GOSSIP_MENU();
        }
        return true;
    }
};
 
void AddSC_npc_promohorda() 
{
    new npc_promohorda(); 
}