/* ScriptData
Name: fake_commandscript
%Complete: 100
Comment: Allows to set fake characters as online or not
Category: commandscripts
EndScriptData */


#include "ScriptPCH.h"
#include "ScriptMgr.h"
#include "AccountMgr.h"
#include "Chat.h"

class fake_commandscript : public CommandScript
{
public:
    fake_commandscript() : CommandScript("fake_commandscript") { }

    ChatCommand* GetCommands() const
    {
        ChatCommand static fakeCommandTable[] =
        {
            { "account",  SEC_GAMEMASTER,   true, &HandleFakeAccount,  "", NULL },
            { "player",   SEC_GAMEMASTER,   true,  &HandleFakePlayer,  "", NULL },
            { NULL,                    0,  false,               NULL,  "", NULL }
        };
        ChatCommand static commandTable[] =
        {
            { "fake",    SEC_GAMEMASTER,  true,                NULL,  "", fakeCommandTable },
            { NULL,                   0,  false,               NULL,  "",             NULL }
        };
        return commandTable;
    }

    static bool HandleFakeAccount(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        char* account = strtok((char*)args, " ");
        char* toggle = strtok(NULL, " ");
        if (!account || !toggle)
            return false;

        std::string accountName = account;
        std::string online = toggle;

        if (!AccountMgr::normalizeString(accountName))
        {
            handler->SetSentErrorMessage(true);
            return false;
        }

        uint32 accountId = AccountMgr::GetId(accountName);
        if (!accountId)
        {
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (online == "on")
        {
            CharacterDatabase.PExecute("UPDATE characters SET online = 2 WHERE account = '%u'", accountId);
            handler->PSendSysMessage("All characters from account %s are now online.", accountName.c_str());
            return true;
        }

        if (online == "off")
        {
            CharacterDatabase.PExecute("UPDATE characters SET online = 0 WHERE account = '%u'", accountId);
            handler->PSendSysMessage("All characters from account %s are now offline.", accountName.c_str());
            return true;
        }

        handler->SetSentErrorMessage(true);
        return false;
    }

    static bool HandleFakePlayer(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        char* name = strtok((char*)args, " ");
        char* toggle = strtok(NULL, " ");
        if (!name || !toggle)
            return false;

        std::string playerName = handler->extractPlayerNameFromLink(name);
        if (playerName.empty())
        {
            handler->SetSentErrorMessage(true);
            return false;
        }

        std::string online = toggle;
        uint64 guid = sObjectMgr->GetPlayerGUIDByName(playerName);
        uint32 accountId = sObjectMgr->GetPlayerAccountIdByGUID(guid);

        if (online == "on")
        {
            CharacterDatabase.PExecute("UPDATE characters SET online = 2 WHERE guid = '%u'", guid);
            handler->PSendSysMessage("Character %s is now online.", playerName.c_str());
            return true;
        }

        if (online == "off")
        {
            CharacterDatabase.PExecute("UPDATE characters SET online = 0 WHERE guid = '%u'", guid);
            handler->PSendSysMessage("Character %s is now offline.", playerName.c_str());
            return true;
        }

        handler->SetSentErrorMessage(true);
        return false;
    }
};

void AddSC_fake_commandscript()
{
    new fake_commandscript();
}