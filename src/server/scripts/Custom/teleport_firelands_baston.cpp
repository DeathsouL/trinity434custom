#include "ScriptMgr.h"
#include "Player.h"

class teleport_firelands_baston : public GameObjectScript
{
    public:

        teleport_firelands_baston(): GameObjectScript("teleport_firelands_baston"){}

        bool OnGossipHello(Player* player, GameObject* go) OVERRIDE
        {
            player->ADD_GOSSIP_ITEM(0, "Ir al yunque de la conflagracion", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+13);
            player->ADD_GOSSIP_ITEM(0, "Ir a la entrada de Firelands", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+14);			
			player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, go->GetGUID());
            return true;
        }

		bool OnGossipSelect(Player* player, GameObject* go, uint32 sender, uint32 action) OVERRIDE
		{
			player->PlayerTalkClass->ClearMenus();
			switch(action)
			{
			case GOSSIP_ACTION_INFO_DEF+13:
				player->TeleportTo(720,455.58f,515.16f,250.8f,0.09f);
				break;
			case GOSSIP_ACTION_INFO_DEF+14:	
				player->TeleportTo(720,-365.81f,205.04f,55.86f,3.06f);	
				break;
			default:
				player->CLOSE_GOSSIP_MENU();
			}
			return true;
		}
};

void AddSC_teleport_firelands_baston()
{
    new teleport_firelands_baston();
}