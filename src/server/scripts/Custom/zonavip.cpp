/*
AREA CHECK PARA LA ZONA VIP
*/
#include "ScriptPCH.h"
 
class Vip_Access: public PlayerScript
{
    public:
        Vip_Access() : PlayerScript("Vip_Access") {}
 
    void OnUpdateZone(Player* player, uint32 /*newZone*/, uint32 /*newArea*/)
    {
    	if (player->GetAreaId() == 268 && !player->GetSession()->IsPremium())
    	{
			player->TeleportTo(571, 5805.69f, 624.99f, 650.75f, 1.85f);
			ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[BW]:|r No puedes entrar a la zona VIP!|r!");
    	}
	}
};
 
void AddSC_Vip_Access()
{
    new Vip_Access();
}